﻿# Assignment 10
Design and implement the following 2D match-3 mini-game.

● Main scene: Lets the player choose between 2 power-ups (bomb: when used in a combo, all nearby
items explode; and freeze: when used in a combo, the timer stops for a few seconds) and start a
new game. Also displays a persistent high score.

● Game scene: Displays an 8x8 grid of items in 6 different colors (plus power-ups) for a match-3
game. The game ends either when the time runs out or there are no possible combinations on the
grid (return to the main screen). Add a pause menu for quitting the game.

Scene transitions and gameplay events must be animated.

Gameplay configuration (ie. points per combo, chance for an item to be a power-up, etc.) must be customizable at editing
time.

Ensure that the project can be tested with the UnityRemote app and built for the Android and iOS platforms

## GETTING STARTED
Try the webgl demo [here](http://nicolacastellani.devplaygrounds.org/assets/static/webgl/polygone/index.html).

You can build for android or iOS.


### Project Structure

* Assets/
    * Artworks/ (all the sounds/images)
    * Plugins/ (external plugins)
    * Resources/ (Scriptable Objects and prefabs)
    * Scenes/
    * Techinal/ (scripts and tests)
    * WebGLTemplates/ (custom templates for webgl exports)

### NOTES

Every graphic assets in game is made by me using Gravit Designer. I've choose to develop this game in TDD.
The sounds/music are from [ZapSplat](https://www.zapsplat.com/) website.

### PREVIEW PACKAGES

I've used Unity ***device simulator*** preview package to simulate different responsive behaviour results.

## Game development method

I've choose to develop this game using TDD method (for *BoardMatcher*, *BoardRefiller*, *BoardGenerator* classes).
So I've created the tests first (you can find them under ***Technical/Tests*** folder), and than develop the logic.

## Gameplay mechanics

1. Board starts with no power ups
2. At least one match exists at start
3. Special tiles (bombs, freeze) are jolly, they match in a combo
4. Freeze stacks on timer
5. The score is recorded only when game is ended

## Main Scene

I've choose to store 2 different high scores for bombs and freeze. The serialized class is ***PlayerData*** which is
stored using **PlayerDataAPI** script (which is also tested) on a local json file.

## Game scene

### ***BoardController***

Handles board, you can set:

* Rows and columns
* Special Tile Probability
* Tile data (using **BoardTileData** scriptable objects)
* Bomb data (using **BoardTileData** scriptable objects)
* Freeze data (using **BoardTileData** scriptable objects)

### ***TimerController***

Handles timer, you can set the ***time amount*** for the game.

### ***ScoreManager***
Handles score, you can set:

* Score per tile
* Combo multiplier (how much the combo affect the score)
* Combo reset time