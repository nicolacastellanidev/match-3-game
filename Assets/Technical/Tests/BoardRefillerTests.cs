﻿using NUnit.Framework;
using Technical.Scripts;
using Technical.Scripts.Board;
using Technical.Scripts.Models;

namespace Technical.Tests
{
    public class BoardRefillerTests
    {
        private readonly int[] seeds = {0, 1, 2, 3, 4, 5};

        [Test]
        public void BoardRefillerWithUpperRow()
        {
            var board = new[,]
            {
                {0, 0, 0, 0},
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {3, 3, 3, 3}
            };
            var result = BoardRefiller.GetNextSeedForPosition(board, new BoardPoint(1, 0), seeds, 0, 0);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(0, board[1, 0]); 
            Assert.AreNotEqual(0, board[0, 0]); 
        }
        
        [Test]
        public void BoardRefillerWithoutUpperRow()
        {
            var board = new[,]
            {
                {0, 0, 0, 0},
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {3, 3, 3, 3}
            };
            BoardRefiller.GetNextSeedForPosition(board, new BoardPoint(0, 0), seeds, 10, 0);
            Assert.AreNotEqual(0, board[0,0]); // returns a different seed
        }
        
        [Test]
        public void BoardRefiller_DropBombs()
        {
            var board = new[,]
            {
                {0, 0, 0, 0},
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {3, 3, 3, 3}
            };
            BoardRefiller.GetNextSeedForPosition(board, new BoardPoint(0, 0), seeds, 100, 0);
            for (var row = 0; row < board.GetLength(0); row++)
            {
                for (var col = 0; col < board.GetLength(1); col++)
                {
                    if (board[row, col] == BoardConstants.BombSeed)
                    {
                        Assert.Pass();
                    }
                }
            }
            Assert.Fail();
        }
        
        [Test]
        public void BoardRefiller_DropFreezes()
        {
            var board = new[,]
            {
                {0, 0, 0, 0},
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {3, 3, 3, 3}
            };
            BoardRefiller.GetNextSeedForPosition(board, new BoardPoint(0, 0), seeds, 0, 1);
            for (var row = 0; row < board.GetLength(0); row++)
            {
                for (var col = 0; col < board.GetLength(1); col++)
                {
                    if (board[row, col] == BoardConstants.FreezeSeed)
                    {
                        Assert.Pass();
                    }
                }
            }
            Assert.Fail();
        }
        
        [Test]
        public void BoardRefiller_ShouldNotDropSpecialSeedOnSpecialSeed()
        {
            var board = new[,]
            {
                {BoardConstants.BombSeed}
            };
            BoardRefiller.GetNextSeedForPosition(board, new BoardPoint(0, 0), seeds, 100, 100);
            if (board[0, 0] == BoardConstants.BombSeed || board[0, 0] == BoardConstants.FreezeSeed)
            {
                Assert.Fail();
            }
            Assert.Pass();
        }
    }
}