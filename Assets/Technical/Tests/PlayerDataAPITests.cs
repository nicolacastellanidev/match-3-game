﻿using NUnit.Framework;
using Technical.Scripts.API;
using Technical.Scripts.Models;

namespace Technical.Tests
{
    public class PlayerDataAPITests
    {
        [Test]
        public void ShouldLoadDefaultData()
        {
            var playerData = PlayerDataAPI.LoadData(true);
            Assert.NotNull(playerData);
        }
        
        [Test]
        public void ShouldSavePlayerData()
        {
            var playerData = new PlayerData {scoreRecordBomb = 1000, scoreRecordFreeze = 1000};
            Assert.AreEqual(true, PlayerDataAPI.SaveData(playerData, true));
            Assert.AreEqual(playerData, PlayerDataAPI.TestData);
        }
        
        [Test]
        public void ShouldReloadPlayerData()
        {
            var playerData = new PlayerData {scoreRecordBomb = 1000, scoreRecordFreeze = 1000};
            Assert.AreEqual(true, PlayerDataAPI.SaveData(playerData, true));
            Assert.AreEqual(playerData, PlayerDataAPI.TestData);
            playerData = PlayerDataAPI.LoadData(true);
            Assert.NotNull(playerData);
            Assert.AreEqual(1000, playerData.scoreRecordBomb);
            Assert.AreEqual(1000, playerData.scoreRecordFreeze);
        }
    }
}