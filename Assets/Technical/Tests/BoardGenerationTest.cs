﻿using NUnit.Framework;
using Technical.Scripts;
using Technical.Scripts.Board;
using UnityEditor.VersionControl;
using UnityEngine;

namespace Technical.Tests
{
    public class BoardGenerationTests
    {
        readonly int[] seeds = {0, 1, 2, 3, 4, 5};
        private int B = BoardConstants.BombSeed;
        const int xSize = 4;
        const int ySize = 4;
        // Tests matrix creation
        [Test]
        public void BoardGeneration()
        {
            var board = BoardGenerator.CreateMatrix(xSize, ySize, seeds, 0.1f);
            Assert.NotNull(board);
            Assert.AreEqual(board.GetLength(0), xSize);
            Assert.AreEqual(board.GetLength(1), ySize);
        }
        
        // Tests matrix creation
        [Test]
        public void BoardGeneration_AllRandHorizontal()
        {
            var board = BoardGenerator.CreateMatrix(xSize, ySize, seeds, 0.1f);
            Assert.NotNull(board);
            
            // there should be max 2 consecutive equals number per row
            for (var row = 0; row < board.GetLength(0); row++)
            {
                for (var col = 1; col < board.GetLength(1) - 1; col++)
                {
                    if (board[row, col] != board[row, col - 1] || board[row, col] != board[row, col + 1]) continue;
                    Debug.Log($"row: {row} col: {col}");
                    Assert.Fail("There are 3 same tile in a row");
                }
            }
            
            Assert.Pass();
        }
        
        // Tests matrix creation
        [Test]
        public void BoardGeneration_AllRandVertical()
        {
            var board = BoardGenerator.CreateMatrix(xSize, ySize, seeds,  0.1f);
            Assert.NotNull(board);
            
            // there should be max 2 consecutive equals number per row
            for (var row = 1; row < board.GetLength(0) - 1; row++)
            {
                for (var col = 0; col < board.GetLength(1) - 2; col++)
                {
                    if (board[row, col] != board[row - 1, col] || board[row, col] != board[row + 1, col]) continue;
                    Debug.Log($"row: {row} col: {col}");
                    Assert.Fail("There are 3 same tile in a column");
                }
            }
            
            Assert.Pass();
        }
        
        [Test]
        public void BoardGeneration_ShouldNotCreateEmptyCells()
        {
            var board = BoardGenerator.CreateMatrix(xSize, ySize, seeds, 0.1f);
            Assert.NotNull(board);
            
            // there should be max 2 consecutive equals number per row
            for (var row = 0; row < board.GetLength(0); row++)
            {
                for (var col = 0; col < board.GetLength(1) - 2; col++)
                {
                    if (board[row, col] >= 0) continue;
                    Debug.Log($"row: {row} col: {col}");
                    Assert.Fail("There is an empty cell");
                }
            }
            
            Assert.Pass();
        }
        
        [Test]
        public void BoardGenerationAllRandom()
        {
            var board1 = BoardGenerator.CreateMatrix(xSize, ySize, seeds, 0.1f);
            var board2 = BoardGenerator.CreateMatrix(xSize, ySize, seeds, 0.1f);
            for (var row = 0; row < xSize; row++)
            {
                for (var col = 0; col < ySize; col++)
                {
                    if (board1[row, col] != board2[row, col])
                    {
                        Assert.Pass();
                    }
                }
            }
            Assert.Fail();
        }
        
        // Tests bombs generation
        [Test]
        public void BoardGeneration_Bombs()
        {
            var board = BoardGenerator.CreateMatrix(xSize, ySize, seeds,  1);
            for (var row = 0; row < ySize; row++)
            {
                for (var col = 0; col < xSize; col++)
                {
                    if (board[row, col] == B)
                    {
                        Assert.Pass();
                    }
                }
            }
            Assert.Fail();
        }
    }
}