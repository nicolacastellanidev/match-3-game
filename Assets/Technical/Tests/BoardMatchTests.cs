﻿using NUnit.Framework;
using Technical.Scripts.Board;
using Technical.Scripts.Models;

namespace Technical.Tests
{
    public class BoardMatchTests
    {
        int[] seeds = {0, 1, 2, 3, 4, 5};
        private int B = BoardConstants.BombSeed;
        private int F = BoardConstants.FreezeSeed;
        const int xSize = 4;
        const int ySize = 4;
        // Tests matrix creation
        [Test]
        public void BoardMatchHorizontalMatch()
        {
            var board = new[,] {{0, 0, 1, 0}};
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 3), new BoardPoint(0, 2));
            Assert.AreEqual(3, result.Count);
            for (var i = 0; i < result.Count; i++)
            {
                if(!result[i].Equals(new BoardPoint(0, i)))
                    Assert.Fail();
            }
            Assert.Pass();
        }
        
        [Test]
        public void BoardMatchHorizontalMatchMoreThan3()
        {
            var board = new[,] {{0, 0, 0, 0}};
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 3), new BoardPoint(0, 2));
            Assert.AreEqual(4, result.Count);
        }
        
        [Test]
        public void BoardMatchHorizontalNoMatch()
        {
            var board = new[,] {{0, 2, 1, 3}};
            int[] from = {0, 3};
            int[] to = {0, 2};
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 3), new BoardPoint(0, 2));
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void BoardMatchHorizontalWithSwipeValid()
        {
            var board = new[,] {{0, 0, 1, 0}};
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 2), new BoardPoint(0, 3));
            Assert.AreEqual(3, result.Count);
            // should works properly on reverse
            result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 3), new BoardPoint(0, 2));
            Assert.AreEqual(3, result.Count);
        }
        
        [Test]
        public void BoardMatchHorizontalMatchWithApply()
        {
            var board = new[,] {{0, 0, 1, 0}};
            BoardMatcher.MatchAll(board, new BoardPoint(0, 3), new BoardPoint(0, 2));
            Assert.AreEqual(0, board[0,0]);
            Assert.AreEqual(0, board[0,1]);
            Assert.AreEqual(0, board[0,2]);
            Assert.AreEqual(1, board[0,3]);
            BoardMatcher.MatchAll(board, new BoardPoint(0, 3), new BoardPoint(0, 2));
            Assert.AreEqual(0, board[0,0]);
            Assert.AreEqual(0, board[0,1]);
            Assert.AreEqual(1, board[0,2]);
            Assert.AreEqual(0, board[0,3]);
        }

        [Test]
        public void BoardMatchHorizontalNotAdjacentCols()
        {
            var board = new[,] {{0, 0, 1, 0}};
            Assert.IsEmpty(BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 1), new BoardPoint(0, 3)));
        }

        [Test]
        public void BoardMatchVerticalValid()
        {
            var board = new[,]
            {
                {0, 2, 1, 0}, 
                {2, 1, 0, 0},
                {0, 3, 1, 4}
            };
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(1, 1), new BoardPoint(1, 2));
            Assert.AreEqual(3, result.Count);
            for (var i = 0; i < result.Count; i++)
            {
                if (!result[i].Equals(new BoardPoint(i, 2)))
                {
                    Assert.Fail();
                }
            }
            // should work reverse
            result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(1, 2), new BoardPoint(1, 1));
            Assert.AreEqual(3, result.Count);
            for (var i = 0; i < result.Count; i++)
            {
                if (!result[i].Equals(new BoardPoint(i, 2)))
                {
                    Assert.Fail();
                }
            }
            
            Assert.Pass();
        }
        
        [Test]
        public void BoardMatchVerticalValidMoreThan3()
        {
            var board = new[,]
            {
                {3, 0, 1, 4}, 
                {0, 1, 0, 4},
                {4, 2, 1, 0},
                {1, 0, 1, 2}
            };
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(1, 1), new BoardPoint(1, 2));
            Assert.AreEqual(4, result.Count);
        }
        
        // issues found with playing
        
        [Test]
        public void BoardMatchVerticalWithAtLeastOneMatchForTile()
        {
            var board = new[,]
            {
                {0, 0, 2, 0}, 
                {0, 1, 0, 0},
                {2, 0, 1, 4},
                {3, 0, 1, 5}
            };
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 2), new BoardPoint(1, 2));
            Assert.AreEqual(4, result.Count);
        }
        
        [Test]
        public void BoardMatchWithCombosAndApplyMatch()
        {
            var board = new[,]
            {
                {6, 5, 1, 5},
                {0, 0, 1, 0},
                {1, 1, 0, 2},
                {2, 3, 4, 3}
            };
            var result = BoardMatcher.MatchAll(board, new BoardPoint(1, 2), new BoardPoint(2, 2));
            Assert.AreEqual(7, result.Count);
        }
        
        [Test]
        public void BoardMatchMatchAllWithNoPositionSwitch()
        {
            var board = new[,]
            {
                {0, 0, 0, 0},
                {1, 1, 1, 1},
                {2, 2, 2, 2},
                {3, 3, 3, 3}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(xSize * ySize, result.Count);
        }

        // ----------------------------------- BOMBS
        [Test]
        public void BoardMatchWithBombs_Horizontally()
        {
            var board = new[,]
            {
                {1, 1, B, 1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(4, result.Count);
        }

        [Test]
        public void BoardMatchWithBombs_Vertically()
        {
            var board = new[,]
            {
                {1},
                {1},
                {B},
                {1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(4, result.Count);
        }
        
        [Test]
        public void BoardMatchWithBombs_NotMatchHorizontally()
        {
            var board = new[,]
            {
                {1, 2, B, 1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(0, result.Count);
        }
        
        [Test]
        public void BoardMatchWithBombs_NotMatchVertically()
        {
            var board = new[,]
            {
                {1},
                {2},
                {B},
                {1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void BoardMatchWithBombs_MatchAroundBomb()
        {
            var board = new[,]
            {
                {0, 3, 4, 2, 3},
                {1, 1, B, 2, 2},
                {2, 3, 5, 5, 4},
                {5, 2, 6, 1, 0}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(7, result.Count);
        }

        [Test]
        public void BoardMatchWithBombs_OnlyInCombo()
        {
            var board = new[,]
            {
                {0, 3, 4, 2},
                {1, 2, B, 1},
                {2, 3, 5, 5},
                {5, 2, 6, 1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(0, result.Count);
        }
        
        // ----------------------------------- FREEZE
        [Test]
        public void BoardMatchWithFreeze_Horizontally()
        {
            var board = new[,]
            {
                {1, 1, F, 1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(4, result.Count);
        }
        
        [Test]
        public void BoardMatchWithFreeze_Vertically()
        {
            var board = new[,]
            {
                {1},
                {1},
                {F},
                {1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(4, result.Count);
        }
        
        [Test]
        public void BoardMatchWithFreeze_NotMatchHorizontally()
        {
            var board = new[,]
            {
                {1, 2, F, 1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(0, result.Count);
        }
        
        [Test]
        public void BoardMatchWithFreeze_NotMatchVertically()
        {
            var board = new[,]
            {
                {1},
                {2},
                {F},
                {1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void BoardMatchWithFreeze_MatchAroundFreeze()
        {
            var board = new[,]
            {
                {0, 3, 4, 2, 3},
                {1, 1, F, 5, 2},
                {2, 3, 5, 5, 4},
                {5, 2, 6, 1, 0}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(3, result.Count);
        }

        [Test]
        public void BoardMatchWithFreeze_OnlyInCombo()
        {
            var board = new[,]
            {
                {0, 3, 4, 2},
                {1, 2, F, 1},
                {2, 3, 5, 5},
                {5, 2, 6, 1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(0, result.Count);
        }
        
        // -------------------------------- GENERIC
        [Test]
        public void BoardMatch_CanMoveOnBottomLeftWithSpecial()
        {
            var board = new[,]
            {
                {0, 1, 4, 2},
                {B, 2, 4, 1},
                {1, 3, 5, 3},
                {2, 6, 1, 1}
            };
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(0, 1), new BoardPoint(0,0));
            Assert.AreEqual(4, result.Count);
        }
        
        [Test]
        public void BoardMatch_NoMoreMoves()
        {
            var board = new[,]
            {
                {0, 2, 3, 4},
                {1, 4, 1, 2},
                {5, 6, 2, 1},
                {8, 7, 4, 3}
            };
            var result = BoardMatcher.CheckIfCanContinue(board);
            Assert.AreEqual(null, result);
        }
        
        [Test]
        public void BoardMatch_MoreMoves()
        {
            var board = new[,]
            {
                {0, 2, 3, 4},
                {1, 4, 1, 1},
                {5, 6, 2, 1},
                {8, 7, 4, 3}
            };
            var result = BoardMatcher.CheckIfCanContinue(board);
            Assert.AreEqual(2, result.Count);
        }
        
        [Test]
        public void BoardMatch_NoMoreMovesWithBomb()
        {
            var board = new[,]
            {
                {0, 1, 2, 3},
                {3, 4, 4, 0},
                {0, 4, B, 5},
                {5, 6, 5, 3}
            };
            var result = BoardMatcher.CheckIfCanContinue(board);
            Assert.AreEqual(null, result);
        }
        
        [Test]
        public void BoardMatch_BombDownWithPossibleMatch()
        {
            var board = new[,]
            {
                {0, 1, 2, 0},
                {1, B, 3, 4},
                {2, 2, 1, 1}
            };
            var result = BoardMatcher.TestSwipeMatch(board, new BoardPoint(1, 1), new BoardPoint(2,1));
            Assert.AreEqual(5, result.Count);
        }
        
        [Test]
        public void BoardMatch_DontMatchWithCross()
        {
            var board = new[,]
            {
                {0, 1, 2, 0},
                {1, B, 3, 4},
                {2, 3, 1, 1}
            };
            var result = BoardMatcher.MatchAll(board, null, null, false);
            Assert.AreEqual(0, result.Count);
        }
    }
}