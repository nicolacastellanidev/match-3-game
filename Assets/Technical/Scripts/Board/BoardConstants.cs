﻿namespace Technical.Scripts.Board
{
    /// <summary>
    /// Global shared board constants
    /// </summary>
    public static class BoardConstants
    {
        public const int BombSeed = 100;
        public const int FreezeSeed = 110;
    }
}