﻿using System;
using System.Collections;
using Technical.Scripts.Models;
using Technical.Scripts.ScriptableObjects;
using Technical.Scripts.Singletons;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Board
{
    [RequireComponent(typeof(AudioSource), typeof(Animator))]
    public class TileController : MonoBehaviour
    {
        #region Variables

        #region Inspector variables
        [HideInInspector] public Vector3 currentPosition;
        #endregion

        #region Public variables
        public BoardPoint Position;
        public int Row => Position.row;
        public int Column => Position.column;
        public UnityAction<TileController> ONClick;
        #endregion

        #region Private variables
        private Guid _id;
        private BoardTileData _data;
        
        private SpriteRenderer _render;
        private Animator _animator;
        private AudioSource _audio;
        
        private bool _reloading = false;
        private bool _exploding = false;
        
        private static readonly int Explode = Animator.StringToHash("explode");
        private static readonly int Initialized = Animator.StringToHash("initialized");
        private static readonly int Changed = Animator.StringToHash("changed");
        private static readonly int ShakeTrigger = Animator.StringToHash("shake");
        private static readonly int HighlightTrigger = Animator.StringToHash("highlight");
        
        private Coroutine _reloadingCo = null;

        #endregion

        #endregion

        #region Unity Lifecycle methods

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _audio = GetComponent<AudioSource>();
            _render = GetComponentInChildren<SpriteRenderer>();
        }
        
        public void OnMouseDown()
        {
#if UNITY_EDITOR
            if (EditorApplication.isRemoteConnected) return;
#endif
            if (Application.platform == RuntimePlatform.Android || 
                Application.platform == RuntimePlatform.IPhonePlayer)
                return;
            if (GameController.gamePaused) return;
            ONClick?.Invoke(this);
        }

        #endregion
        #region Class utilities

        /// <summary>
        /// Overrides object ToString method
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return $"#{_id}[{Position.row},{Position.column}]";
        }
        
        /// <summary>
        /// Check if current tile is equal to another
        /// </summary>
        /// <param name="tile">TileController</param>
        /// <returns>bool</returns>
        public bool Equals(TileController tile)
        {
            return _id == tile._id;
        }

        #endregion

        #region Class methods

        /// <summary>
        /// Handles tile selection
        /// </summary>
        public void Select()
        {
            _render.sprite = _data.spriteOnSelection;
            PlayClip(_data.soundOnSelect);
        }

        /// <summary>
        /// Handles Tile deselection
        /// </summary>
        public void Deselect()
        {
            _render.sprite = _data.sprite;
        }
        
        
        /// <summary>
        /// Initializes tile
        /// </summary>
        /// <param name="row">int</param>
        /// <param name="column">int</param>
        /// <param name="data">BoardTileData</param>
        /// <param name="initAfter">float</param>
        public void Init(int row, int column, BoardTileData data, float initAfter)
        {
            Position = new BoardPoint(row, column);
            _id = Guid.NewGuid();
            gameObject.name = $"Tile-{row}-{column}";
            currentPosition = transform.position;
            StartCoroutine(SetData(data, initAfter));
        }

        /// <summary>
        /// Moves physically a tile from a position to another (swipe)
        /// </summary>
        /// <param name="position">Vector3</param>
        /// <param name="row">int</param>
        /// <param name="column">int</param>
        public void MoveTo(Vector3 position, int row, int column)
        {
            Position = new BoardPoint(row, column);
            gameObject.name = $"Tile-{row}-{column}";
            currentPosition = position;
            StartCoroutine(MoveToPosition());
        }
        
        /// <summary>
        /// Performs shake animation
        /// </summary>
        /// <param name="playSound">bool</param>
        public void Shake(bool playSound = false)
        {
            _animator.SetTrigger(ShakeTrigger);
            if(playSound) PlayClip(_data.soundOnShake);
        }

        /// <summary>
        /// Set tile data
        /// </summary>
        /// <param name="data">BoardTileData</param>
        /// <param name="initAfter">float</param>
        private IEnumerator SetData(BoardTileData data, float initAfter)
        {
            yield return new WaitForSeconds(initAfter);
            UpdateData(data);
            _animator.SetBool(Initialized, true);
        }

        /// <summary>
        /// Moves object to target position
        /// </summary>
        private IEnumerator MoveToPosition()
        {
            while (transform.position != currentPosition)
            {
                transform.position = Vector3.MoveTowards(transform.position, currentPosition, 0.1f);
                yield return new WaitForSeconds(0.01f);
            }
        }
        
        /// <summary>
        /// Highlight animation
        /// </summary>
        public void Highlight()
        {
            _animator.SetBool(HighlightTrigger, true);
        }

        /// <summary>
        /// Removes highlight animation
        /// </summary>
        public void RemoveHighlight()
        {
            _animator.SetBool(HighlightTrigger, false);
        }

        /// <summary>
        /// Called after tile match
        /// </summary>
        /// <param name="delay">float</param>
        public void Matched(float delay)
        {
            if (_exploding)
                return;
            TileAnimationLock.Lock();
            StopCoroutine(ExplodeAnimation(delay));
            StartCoroutine(ExplodeAnimation(delay));
        }

        /// <summary>
        /// Explode animation
        /// </summary>
        /// <param name="waitFor">float</param>
        private IEnumerator ExplodeAnimation(float waitFor)
        {
            if (_data.seed == BoardConstants.FreezeSeed)
            {
                GameController.FreezeTime(_data.amount);
            }
            _exploding = true;
            yield return new WaitForSeconds(waitFor); // wait for animation
            PlayClip(_data.soundOnPop);
            _animator.SetTrigger(Explode);
            yield return new WaitForSeconds(0.5f); // wait for animation complete
            TileAnimationLock.Unlock();
            _exploding = false;
        }

        /// <summary>
        /// Updates data after refill
        /// </summary>
        /// <param name="data">BoardTileData</param>
        /// <param name="waitFor">float</param>
        public void SetNewData(BoardTileData data, float waitFor = 0f)
        {
            if (_reloading)
            {
                Debug.LogWarning("Reloading a reloading tile");
            }

            if (_reloading)
            {
                StopCoroutine(_reloadingCo);
                TileAnimationLock.Unlock();
            }
            TileAnimationLock.Lock();
            _reloadingCo = StartCoroutine(ReloadTile(data, waitFor));
        }

        /// <summary>
        /// Reload tile animation
        /// </summary>
        /// <param name="data">BoardTileData</param>
        /// <param name="waitFor">float</param>
        private IEnumerator ReloadTile(BoardTileData data, float waitFor)
        {
            _reloading = true;
            yield return new WaitForSeconds(waitFor); // wait for animation
            UpdateData(data);
            _animator.SetTrigger(Changed);
            yield return new WaitForSeconds(1f); // wait for animation
            _reloading = false;
            TileAnimationLock.Unlock();
        }

        /// <summary>
        /// Updates tile data, and animator layer weight
        /// </summary>
        /// <param name="data">BoardTileData</param>
        private void UpdateData(BoardTileData data)
        {
            _data = data;
            _render.sprite = data.sprite;
            _animator.SetLayerWeight(1, data.seed == BoardConstants.BombSeed ? 0 : 1);
            _animator.SetLayerWeight(2, data.seed == BoardConstants.BombSeed ? 1 : 0);
        }

        /// <summary>
        /// Plays audio clip
        /// </summary>
        /// <param name="clip">AudioClip</param>
        private void PlayClip(AudioClip clip)
        {
            if(_audio.isPlaying) _audio.Stop();
            _audio.PlayOneShot(clip);
        }

        #endregion
    }
}