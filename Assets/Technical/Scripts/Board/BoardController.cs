﻿using System.Collections.Generic;
using Technical.Scripts.Models;
using Technical.Scripts.ScriptableObjects;
using Technical.Scripts.Singletons;
using UnityEngine;

namespace Technical.Scripts.Board
{
    public class BoardController : MonoBehaviour
    {
        #region Variables
        [Space(10)]
        [Header("Board management")]
        [SerializeField] private int rows = 4;
        [SerializeField] private int columns = 4;
        [SerializeField] private TileController tile;
        [SerializeField] [Range(0, 100)] private float specialTileProbability = 10;
        [SerializeField] public List<BoardTileData> tileDatas = new List<BoardTileData>();
        
        [Space(10)]
        [Header("Special seeds management")]
        [SerializeField] private BoardTileData bombData = null;
        [SerializeField] private BoardTileData freezeData = null;
        
        private int[,] _board;
        private readonly int[] _seeds = {0, 1, 2, 3, 4, 5};
        private TileController[,] _tiles;
        private Vector2 _tileSize;
        private TileController _selectedTile;
        private bool _useBombs;
        
        private float _bombProbability;
        private float _freezeProbability;
        
        private List<BoardPoint> _tilesToRefill = new List<BoardPoint>();
        private readonly List<TileController> _highlightedTiles = new List<TileController>();
        #endregion

        #region Unity lifecycle methods

        private void Start()
        {
            _useBombs = GameController.useBombs;
            _bombProbability = _useBombs ? specialTileProbability/100f : 0;
            _freezeProbability = !_useBombs ? specialTileProbability/100f : 0;
            _tileSize = tile.GetComponentInChildren<SpriteRenderer>().bounds.size;
            GameController.ONSceneLoaded += GenerateBoard;
            GameController.ONStopMatching += StopAll;
            TileAnimationLock.lockFree += RefillAndMatch;
        }

        private void OnDestroy()
        {
            GameController.ONSceneLoaded -= GenerateBoard;
            GameController.ONStopMatching -= StopAll;
            TileAnimationLock.lockFree -= RefillAndMatch;
        }

        #endregion

        #region Board management
        /// <summary>
        /// Creates board and tiles
        /// </summary>
        private void GenerateBoard()
        {
            GameController.ONSceneLoaded -= GenerateBoard;
            _board = BoardGenerator.CreateMatrix(
                rows, 
                columns, 
                _seeds, 
                0
            );
            GenerateTiles();
        }

        /// <summary>
        /// Generates tile based on seeds
        /// </summary>
        private void GenerateTiles()
        {
            _tiles = new TileController[rows, columns];
            var position = transform.position;
            var startX = position.x;
            var startY = position.y;
            var mediumCol = (columns - 1) / 2f;
            var mediumRow = (rows - 1) / 2f;
            for (var rowIndex = 0; rowIndex < rows; rowIndex++)
            {
                for (var colIndex = 0; colIndex < columns; colIndex++)
                {
                    var seed = _board[rowIndex, colIndex];
                    var newData = GetDataForSeed(seed); // get correct card data
                    var nextPosition = new Vector3(startX + _tileSize.x * colIndex, (startY - _tileSize.y * rowIndex));
                    var newTile = Instantiate(tile, nextPosition, tile.transform.rotation, transform);
                    newData.seed = seed;
                    newTile.Init(rowIndex, colIndex, newData, Mathf.Abs(mediumCol - colIndex) * 0.25f + Mathf.Abs(mediumRow - rowIndex) * 0.25f);
                    newTile.ONClick += TileClick;
                    _tiles[rowIndex, colIndex] = newTile;
                }
            }
        }
        #endregion

        #region Tile management
        /// <summary>
        /// Handle click on tile
        /// </summary>
        /// <param name="tileClicked">TileController</param>
        public void TileClick(TileController tileClicked)
        {
            // if (TileAnimationLock.Locked) return;
            if (_selectedTile == null)
            {
                _selectedTile = tileClicked;
                tileClicked.Select();
            }
            else if(_selectedTile.Equals(tileClicked))
            {
                _selectedTile.Deselect();
                _selectedTile = null;
            }
            else
            {
                CheckMatch(tileClicked);
            }
        }

        /// <summary>
        /// Checks if swapping tiles creates a match
        /// </summary>
        /// <param name="targetTile">TileController</param>
        private void CheckMatch(TileController targetTile)
        {
            if (BoardMatcher.TestSwipeMatch(_board, targetTile.Position, _selectedTile.Position).Count == 0)
            {
                // no match
                _selectedTile.Deselect();
                _selectedTile.Shake(true);
                targetTile.Shake();
                _selectedTile = null;
            }
            else
            {
                // can move
                var selectedTilePosition = _selectedTile.currentPosition;
                var selectedTileBoardPosition = _selectedTile.Position;
                var matches = BoardMatcher.MatchAll(_board, targetTile.Position, _selectedTile.Position);
                
                _tiles[_selectedTile.Row, _selectedTile.Column] = targetTile;
                _tiles[targetTile.Row, targetTile.Column] = _selectedTile;
                
                _selectedTile.MoveTo(targetTile.currentPosition, targetTile.Row, targetTile.Column);
                targetTile.MoveTo(selectedTilePosition, selectedTileBoardPosition.row, selectedTileBoardPosition.column);
                
                _selectedTile.Deselect();
                _selectedTile = null;
                
                ScoreController.ComboMatched(matches.Count);
                PerformMatch(matches);
            }
        }

        /// <summary>
        /// Match any combo in board
        /// </summary>
        private void MatchAll()
        {
            if (GameController.gamePaused) return;

            var availableMatches = BoardMatcher.CheckIfCanContinue(_board);
            if (availableMatches == null) // no possible matches, end game
            {
                GameController.EndGame(true);
                return;
            }
            var matches = BoardMatcher.MatchAll(_board, null, null, false);
            if (matches.Count <= 0)
            {
                foreach (var tilePos in availableMatches)
                {
                    _tiles[tilePos.row, tilePos.column].Highlight();
                    _highlightedTiles.Add(_tiles[tilePos.row, tilePos.column]);
                }

                return;
            }
            ScoreController.ComboMatched(matches.Count);
            PerformMatch(matches);
        }
        
        /// <summary>
        /// Checks if there are tiles to refill, else tries to match all combos
        /// </summary>
        private void RefillAndMatch()
        {
            if (_tilesToRefill.Count == 0)
            {
                MatchAll();
                return;
            }
            PerformRefill();
        }

        /// <summary>
        /// Handles match
        /// </summary>
        /// <param name="matches">IEnumerable<BoardPoint></param>
        private void PerformMatch(IEnumerable<BoardPoint> matches)
        {
            ClearHighlightedTiles();
            if(_selectedTile != null) _selectedTile.Deselect();
            _selectedTile = null;
            var count = 0;
            foreach (var match in matches)
            {
                var nextTile = _tiles[match.row, match.column];
                _tilesToRefill.AddRange(BoardRefiller.GetNextSeedForPosition(_board, nextTile.Position, _seeds, _bombProbability, _freezeProbability));
                nextTile.Matched(++count * 0.1f);
            }

            _tilesToRefill = BoardUtils.Distinct(_tilesToRefill);
        }
        
        /// <summary>
        /// Refill touched tiles
        /// </summary>
        private void PerformRefill()
        {
            foreach (var tilePosition in _tilesToRefill)
            {
                var currentTile = _tiles[tilePosition.row, tilePosition.column];
                var seed = _board[tilePosition.row, tilePosition.column];
                var newData = GetDataForSeed(seed); // get correct card data
                newData.seed = seed;
                currentTile.SetNewData(newData);
            }
            _tilesToRefill.Clear();
        }

        /// <summary>
        /// Returns correct data based on seed
        /// </summary>
        /// <param name="seed">int</param>
        /// <returns>BoardTileData</returns>
        private BoardTileData GetDataForSeed(int seed)
        {
            switch (seed)
            {
                case BoardConstants.BombSeed:
                    return bombData;
                case BoardConstants.FreezeSeed:
                    return freezeData;
                default:
                    return tileDatas[seed];
            }
        }
        private void ClearHighlightedTiles()
        {
            foreach (var highlightedTile in _highlightedTiles)
            {
                highlightedTile.RemoveHighlight();
            }
        }
        #endregion

        #region Controller utils

        /// <summary>
        /// Stops all coroutines and invokes
        /// </summary>
        private void StopAll()
        {
            StopAllCoroutines();
            Destroy(gameObject);
        }

        #endregion
    }
}
