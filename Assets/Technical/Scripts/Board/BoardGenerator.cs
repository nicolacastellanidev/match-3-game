﻿using System.Collections.Generic;
using UnityEngine;

namespace Technical.Scripts.Board
{
    public static class BoardGenerator
    {
        /// <summary>
        /// Creates a matrix of random int with the following rules:
        /// 1. there must be max 2 consecutive seeds
        /// </summary>
        /// <param name="xSize">int</param>
        /// <param name="ySize">int</param>
        /// <param name="seeds">int[]</param>
        /// <param name="bombDropProbability">float</param>
        /// <returns>int[,]</returns>
        public static int[,] CreateMatrix(int xSize, int ySize, int[] seeds, float bombDropProbability)
        {
            var result = new int[xSize,ySize];
            var seedsCount = new int[seeds.Length]; // count each seeds count on a row
            for (var row = 0; row < ySize; row++)
            {
                for (var col = 0; col < xSize; col++)
                {
                    var currentSeed = GetRandomSeedsByRow(
                        seeds.Shuffle(),
                        seedsCount,
                        row > 1 ? result[row-1, col] : -1, 
                        bombDropProbability
                        );
                    result[row, col] = currentSeed;
                }
                seedsCount = new int[seeds.Length];
            }
            return result;
        }

        /// <summary>
        /// Returns a random seeds according to it's current repeat rate on row
        /// </summary>
        /// <param name="seeds">int[]</param>
        /// <param name="seedsCount">int[,]</param>
        /// <param name="previousRowColSeed">int</param>
        /// <param name="bombDropProbability">float</param>
        /// <returns>int</returns>
        private static int GetRandomSeedsByRow(IEnumerable<int> seeds, IList<int> seedsCount, int previousRowColSeed, float bombDropProbability)
        {
            if(Random.Range(0f, 1f) <= bombDropProbability)
            {
                return BoardConstants.BombSeed;
            }
            // get a random seed
            foreach (var seed in seeds)
            {
                if (seed == previousRowColSeed)
                {
                    continue;
                }
                if (seedsCount[seed] >= 2)
                {
                    seedsCount[seed] = 0; // repeat
                    continue;
                }
                seedsCount[seed] += 1;
                return seed;
            }

            return -1;
        }
    }
}