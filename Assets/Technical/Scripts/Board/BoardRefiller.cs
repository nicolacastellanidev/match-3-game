﻿using System.Collections.Generic;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.Board
{
    public static class BoardRefiller
    {
        /// <summary>
        /// Refill entire column and shifts down
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <param name="position">BoardPosition</param>
        /// <param name="seeds">int[]</param>
        /// <param name="bombSeedProbability">float</param>
        /// <param name="freezeSeedProbability">float</param>
        /// <returns>Filled tiles positions</returns>
        public static List<BoardPoint> GetNextSeedForPosition(
            int[,] board, BoardPoint position, int[] seeds, float bombSeedProbability, float freezeSeedProbability
        ) {
            var result = new List<BoardPoint>();
            var currentSeed = board[position.row, position.column];
            
            if (currentSeed < BoardConstants.BombSeed)
            {
                var pick = Random.Range(0.01f, 1f);
                if (pick <= bombSeedProbability || pick <= freezeSeedProbability) // change to drop a bomb instead of shifting down
                {
                    result.Add(position);
                    board[position.row, position.column] = pick <= bombSeedProbability ? BoardConstants.BombSeed : BoardConstants.FreezeSeed;
                    return result;
                }   
            }

            if (position.row == 0)
            {
                // first row
                result.Add(position);
                board[position.row, position.column] = GetRandomSeed(seeds.Shuffle(), board[position.row, position.column]);
                return result;
            }

            var currentRow = position.row;
            var currentColumn = position.column;
            while (currentRow > 0)
            {
                result.Add(new BoardPoint(currentRow, currentColumn));
                board[currentRow, currentColumn] = board[currentRow - 1, currentColumn]; // shift down
                currentRow--;
            }

            result.Add(new BoardPoint(currentRow, currentColumn));
            board[currentRow, currentColumn] = GetRandomSeed(seeds, board[currentRow, currentColumn]);
            return BoardUtils.Distinct(result);
        }

        /// <summary>
        /// Returns a random seed different from current
        /// </summary>
        /// <param name="seeds">IReadOnlyList<int></param>
        /// <param name="current">int</param>
        /// <param name="specialSeedProbability">float</param>
        /// <returns>A new random seed</returns>
        private static int GetRandomSeed(IReadOnlyList<int> seeds, int current)
        {
            var nextSeed = current;
            while (nextSeed == current)
            {
                nextSeed = seeds[Random.Range(0, seeds.Count - 1)];
            }

            return nextSeed;
        }
    }
}