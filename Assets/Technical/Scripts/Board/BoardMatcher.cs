﻿using System;
using System.Collections.Generic;
using System.Linq;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.Board
{
    /// <summary>
    /// Handles matches in board, or around a single tile
    /// </summary>
    public static class BoardMatcher
    {
        /// <summary>
        /// Find all matches in board, and switches seeds points
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <param name="from">BoardPoint</param>
        /// <param name="to">BoardPoint</param>
        /// <param name="swapSeeds">bool</param>
        /// <returns></returns>
        public static List<BoardPoint> MatchAll(int[,] board, BoardPoint from, BoardPoint to, bool swapSeeds = true)
        {
            var result = new List<BoardPoint>();
            var rows = board.GetLength(0);
            var columns = board.GetLength(1);

            if (swapSeeds) SwapSeeds(board, from, to); // swap seeds positions

            for (var row = 0; row < rows; row++)
            {
                result.AddRange(CheckRow(board, row)); // check all rows
            }

            for (var col = 0; col < columns; col++)
            {
                result.AddRange(CheckColumn(board, col) ?? Array.Empty<BoardPoint>()); // check all columns
            }

            return BoardUtils.Distinct(result);
        }

        /// <summary>
        /// Check if swipe will match
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>List<BoardPoint></returns>
        public static List<BoardPoint> TestSwipeMatch(int[,] board, BoardPoint from, BoardPoint to)
        {
            var tempBoard = (int[,]) board.Clone();
            Debug.Assert(tempBoard != null, nameof(board) + " != null");
            var rowDiff = Mathf.Abs(from.row - to.row);
            var colDiff = Mathf.Abs(from.column - to.column);
            // not adjacent rows
            if (rowDiff > 1) return new List<BoardPoint>();
            switch (rowDiff)
            {
                case 0 when colDiff > 1: // same row but not adjacent col
                    return new List<BoardPoint>();
                case 1 when colDiff > 0: // different row and different column
                    return new List<BoardPoint>();
            }

            // then check if there is a match on row or column
            var result = MatchSingleTile(tempBoard, from, to);
            result.AddRange(MatchSingleTile(tempBoard, to, from));
            return BoardUtils.Distinct(result);
        }

        /// <summary>
        /// Try to find any match around board
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <returns>bool</returns>
        public static List<BoardPoint> CheckIfCanContinue(int[,] board)
        {
            var rows = board.GetLength(0);
            var columns = board.GetLength(1);
            var check = new List<BoardPoint>();
            for (var row = 0; row < rows - 1; row++)
            {
                for (var col = 0; col < columns; col++)
                {
                    if (TestSwipeMatch(board, new BoardPoint(row, col), new BoardPoint(row + 1, col)).Count > 2)
                    {
                        return new List<BoardPoint>{new BoardPoint(row, col), new BoardPoint(row + 1, col)};
                    }
                }
            }

            for (var col = 0; col < columns - 1; col++)
            {
                for (var row = 0; row < rows; row++)
                {
                    if (TestSwipeMatch(board, new BoardPoint(row, col), new BoardPoint(row, col + 1)).Count > 2)
                    {
                        return new List<BoardPoint>{new BoardPoint(row, col), new BoardPoint(row, col + 1)};
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Swaps tiles, and check if they match
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <param name="from">BoardPoint</param>
        /// <param name="to">BoardPoint</param>
        /// <returns>List<BoardPoint></returns>
        private static List<BoardPoint> MatchSingleTile(int[,] board, BoardPoint from, BoardPoint to)
        {
            var tempBoard = (int[,]) board.Clone();
            // try to swap first
            SwapSeeds(tempBoard, from, to);
            // then check if there is a match on row or column
            var result = CheckRow(tempBoard, to.row);
            result.AddRange(CheckColumn(tempBoard, to.column) ?? Array.Empty<BoardPoint>());
            return result;
        }

        /// <summary>
        /// Check matches in a row
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <param name="rowIndex">int</param>
        /// <returns>List<BoardPoint></returns>
        private static List<BoardPoint> CheckRow(int[,] board, int rowIndex)
        {
            var result = new List<BoardPoint>();
            var count = 0;
            var lastSeed = -1;
            var columns = board.GetLength(1);
            for (var column = 1; column < columns; column++)
            {
                var currentSeed = board[rowIndex, column];
                if (currentSeed < BoardConstants.BombSeed && currentSeed != lastSeed)
                {
                    if (lastSeed > -1) count = 0;
                    lastSeed = currentSeed;
                }

                if (SeedsMatch(board[rowIndex, column], board[rowIndex, column - 1])) count++;
                else count = 0;
                if (count == 2)
                {
                    if (!SeedsMatch(board[rowIndex, column - 2], board[rowIndex, column]))
                    {
                        count--;
                        continue; // check for bombs
                    }

                    result.Add(new BoardPoint(rowIndex, column - 2));
                    result.Add(new BoardPoint(rowIndex, column - 1));
                    result.Add(new BoardPoint(rowIndex, column));
                }
                else if (count > 2)
                {
                    result.Add(new BoardPoint(rowIndex, column));
                }
            }

            result = BoardUtils.Distinct(result);
            return result.Count < 3 ? new List<BoardPoint>() : CheckBomb(board, result);
        }

        /// <summary>
        /// Check matches in a column
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <param name="rowIndex">int</param>
        /// <returns>List<BoardPoint></returns>
        // @todo check row and column are identical. Find a way to use a single method
        private static IEnumerable<BoardPoint> CheckColumn(int[,] board, int colIndex)
        {
            var count = 0;
            var result = new List<BoardPoint>();
            var lastSeed = -1;
            var rows = board.GetLength(0);
            for (var row = 1; row < rows; row++)
            {
                var currentSeed = board[row, colIndex];
                if (currentSeed < BoardConstants.BombSeed && currentSeed != lastSeed)
                {
                    if (lastSeed > -1) count = 0;
                    lastSeed = currentSeed;
                }

                if (SeedsMatch(board[row, colIndex], board[row - 1, colIndex])) count++;
                else count = 0;
                if (count == 2)
                {
                    if (!SeedsMatch(board[row - 2, colIndex], board[row, colIndex]))
                    {
                        count--;
                        continue; // check for bombs
                    }

                    result.Add(new BoardPoint(row - 2, colIndex));
                    result.Add(new BoardPoint(row - 1, colIndex));
                    result.Add(new BoardPoint(row, colIndex));
                }
                else if (count > 2)
                {
                    result.Add(new BoardPoint(row, colIndex));
                }
            }

            return result.Count < 3 ? new List<BoardPoint>() : CheckBomb(board, result);
        }

        /// <summary>
        /// Check if two seeds match are matchable
        /// </summary>
        /// <param name="seedOne">int</param>
        /// <param name="seedTwo">int</param>
        /// <returns>bool</returns>
        private static bool SeedsMatch(int seedOne, int seedTwo)
        {
            return seedOne == seedTwo || seedOne >= BoardConstants.BombSeed || seedTwo >= BoardConstants.BombSeed;
        }

        /// <summary>
        /// Adds to current match the tiles around bomb
        /// </summary>
        /// <param name="board">int[,]</param>
        /// <param name="results">List<BoardPoint></param>
        /// <returns></returns>
        private static List<BoardPoint> CheckBomb(int[,] board, List<BoardPoint> results)
        {
            var maxRow = board.GetLength(0) - 1;
            var maxCol = board.GetLength(1) - 1;
            var added = new List<BoardPoint>();
            foreach (var point in results.Where(point => board[point.row, point.column] == BoardConstants.BombSeed))
            {
                // adds neighbors
                if (point.row > 0) added.Add(new BoardPoint(point.row - 1, point.column));
                if (point.row < maxRow) added.Add(new BoardPoint(point.row + 1, point.column));
                if (point.column > 0) added.Add(new BoardPoint(point.row, point.column - 1));
                if (point.column < maxCol) added.Add(new BoardPoint(point.row, point.column + 1));
            }

            results.AddRange(added);
            return BoardUtils.Distinct(results);
        }

        /// <summary>
        /// Swaps seeds in board
        /// </summary>
        /// <param name="board">int [,]</param>
        /// <param name="from">BoardPoint</param>
        /// <param name="to">BoardPoint</param>
        private static void SwapSeeds(int[,] board, BoardPoint from, BoardPoint to)
        {
            var fromTile = board[from.row, from.column];
            board[from.row, from.column] = board[to.row, to.column];
            board[to.row, to.column] = fromTile;
        }
    }
}