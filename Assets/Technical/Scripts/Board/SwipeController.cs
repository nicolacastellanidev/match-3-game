﻿using System;
using UnityEngine;

namespace Technical.Scripts.Board
{
    public class SwipeController : MonoBehaviour
    {
        [SerializeField] private LayerMask whatIsTouchable;

        private Camera _cam;
        private TileController _startTile;
        private TileController _endTile;
        private BoardController _board;

        private void Awake()
        {
            _cam = Camera.main;
            _board = GetComponentInParent<BoardController>();
            if (_cam) return;
            Debug.LogError("No main Camera found");
            Destroy(this);
        }

        private void Update()
        {
            if (Input.touchCount <= 0) return;
            HandleTouch();
        }

        private void HandleTouch()
        {
            var touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _startTile = CheckTouchRay(touch);
                    if (_startTile)
                    {
                        _board.TileClick(_startTile);
                    }
                    break;
                case TouchPhase.Moved:
                    break;
                case TouchPhase.Ended:
                    _endTile = CheckTouchRay(touch);
                    if (_endTile)
                    {
                        _board.TileClick(_endTile);
                    }
                    else if(_startTile)
                    {
                        // prevent selection keep
                        _board.TileClick(_startTile);
                    }

                    _startTile = null;
                    _endTile = null;
                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Canceled:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private TileController CheckTouchRay(Touch touch)
        {
            var point = _cam.ScreenToWorldPoint(touch.position);
            var hit = Physics2D.OverlapPoint(point,whatIsTouchable);

            if (!hit) return null;
            print($"Hitted {hit.transform.gameObject.name}");
            return hit.transform.gameObject.GetComponent<TileController>();

        }
    }
}
