﻿using System.Collections;
using Technical.Scripts.Singletons;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    /// <summary>
    /// Handles loading screen
    /// </summary>
    public class LoadingScreenController : MonoBehaviour
    {
        #region Variables

        [SerializeField] private Text introText;
        private static LoadingScreenController instance;
        private Animator _animator;
        private static readonly int ShowTrigger = Animator.StringToHash("show");
        private static readonly int HideTrigger = Animator.StringToHash("hide");

        #endregion
        private void Awake()
        {
            if (instance != null && instance != this)
                Destroy(gameObject);
            else
            {
                instance = this;
                DontDestroyOnLoad(this);
            }

            _animator = GetComponent<Animator>();
        }

        
        /// <summary>
        /// Set show animation and load scene async
        /// </summary>
        /// <param name="sceneToLoad">int</param>
        public static void Show(int sceneToLoad)
        {
            // @todo move these strings to a dictionary, and maybe randomize a little bit
            switch (sceneToLoad)
            {
                case 0:
                    instance.introText.text = "Remember, you will lose you records if you don't complete the level.";
                    break;
                case 1: 
                    instance.introText.text = GameController.useBombs
                        ? "Bombs are cool, they make everything crossing them explode after being matched."
                        : "Freeze is fantastic, gain more seconds to play after being matched.";
                    break;
            }
            instance._animator.SetTrigger(ShowTrigger);
            instance.StartCoroutine(LoadSceneAsync(sceneToLoad));
        }
        
        /// <summary>
        /// Hide animation trigger
        /// </summary>
        private static void Hide()
        {
            instance._animator.SetTrigger(HideTrigger);
        }
        
        /// <summary>
        /// Load scene
        /// </summary>
        /// <param name="sceneIndex"></param>
        /// <returns></returns>
        private static IEnumerator LoadSceneAsync(int sceneIndex)
        {
            yield return new WaitForSeconds(5);
            
            var asyncLoad = SceneManager.LoadSceneAsync(sceneIndex);
            
            while (!asyncLoad.isDone)
            {
                yield return null;
            }
            
            Hide();

            yield return new WaitForSeconds(1);
            GameController.AfterSceneLoaded();
        }
    }
}