﻿using Technical.Scripts.Singletons;
using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    public class GameUIController : MonoBehaviour
    {
        #region Variables

        [Space(10)] [Header("Timer settings")] [SerializeField]
        private Text timerText;

        [SerializeField] private GameObject freezeEffect;

        [Space(10)] [Header("Score Settings")] [SerializeField]
        private ScoreCounterController scoreController;

        [SerializeField] private GameObject scoreRecordLabel;

        [Space(10)] [Header("Combo Settings")] [SerializeField]
        private ComboCounterController comboCounter;

        [Space(10)]
        [Header("End Game Settings")]
// @todo these settings should be handled by a specific PauseMenuUIController script
        [Header("Pause Menu Settings")]
        [SerializeField]
        private GameObject pauseMenuPanel;

        [SerializeField] private Text pauseMenuTitle;
        [SerializeField] private Text pauseMenuScore;
        [SerializeField] private Text endGameReason;
        private bool _isPauseMenuActive = false;

        #endregion

        #region Unity lifecycle

        private void Start()
        {
            ScoreController.ONScoreUpdate += UpdateScore;
            ScoreController.ONComboUpdate += UpdateCombo;
            GameController.ONGameEnded += ShowEndGameUI;
            GameController.timer.ONTimerFreeze += OnTimerFreeze;
            pauseMenuPanel.SetActive(false);
            UpdateScore(0);
        }
        
        private void OnDestroy()
        {
            ScoreController.ONScoreUpdate -= UpdateScore;
            ScoreController.ONComboUpdate -= UpdateCombo;
            GameController.ONGameEnded -= ShowEndGameUI;
            if (GameController.timer) GameController.timer.ONTimerFreeze -= OnTimerFreeze;
        }

        #endregion

        #region Class Methods

        /// <summary>
        /// Event called after freeze match, enables freeze effect
        /// </summary>
        /// <param name="amount">float</param>
        private void OnTimerFreeze(float amount)
        {
            freezeEffect.SetActive(amount > 0);
        }

        /// <summary>
        /// Handles pause menu visibility
        /// </summary>
        public void TogglePauseMenu()
        {
            _isPauseMenuActive = !_isPauseMenuActive;
            if (_isPauseMenuActive)
            {
                // @todo add dictionary for string
                endGameReason.text = "";
                pauseMenuTitle.text = "Game Paused";
                pauseMenuScore.text = "";
            }

            pauseMenuPanel.SetActive(_isPauseMenuActive);
            // @todo cross dependency
            GameController.TogglePause(pauseMenuPanel.activeSelf);
        }

        /// <summary>
        /// Update timer text after timer tick
        /// </summary>
        /// <param name="current">float</param>
        public void UpdateTimerText(float current)
        {
            timerText.text = $"{(int) current / 60}:{(int) current % 60:00}";
        }

        /// <summary>
        /// Called when game ends
        /// </summary>
        /// <param name="noMatch">bool</param>
        public void ShowEndGameUI(bool noMatch)
        {
            // @todo add dictionary for strings
            endGameReason.text = noMatch ? "No more matches." : "Time ended.";
            pauseMenuTitle.text = "Game Over";
            pauseMenuScore.text = $"Your final score is: {ScoreController.score}";
            pauseMenuPanel.SetActive(true);
        }

        /// <summary>
        /// Calls GameController BackToMainScene
        /// </summary>
        public void BackToHomePage()
        {
            GameController.BackToMainScene();
        }

        /// <summary>
        /// Calls GameController ReloadGameScene
        /// </summary>
        public void RestartGame()
        {
            GameController.ReloadGameScene();
        }
        
        /// <summary>
        /// Updates score label and shows Record label if current score is record
        /// </summary>
        /// <param name="amount">int</param>
        private void UpdateScore(int amount)
        {
            if (!scoreRecordLabel.activeSelf && (
                GameController.useBombs
                    ? amount > GameController.playerData.scoreRecordBomb
                    : amount > GameController.playerData.scoreRecordFreeze
            ))
            {
                // new record!
                scoreRecordLabel.SetActive(true);
            }

            scoreController.UpdateText(amount.ToString());
        }

        /// <summary>
        /// Updates combo text
        /// </summary>
        /// <param name="amount"></param>
        private void UpdateCombo(int amount)
        {
            comboCounter.UpdateCombo(amount);
        }

        #endregion
    }
}