﻿using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    /// <summary>
    /// Handles combo UI
    /// </summary>
    public class ComboCounterController : MonoBehaviour
    {

        #region Variables

        [SerializeField] private Text comboText;
        [SerializeField] private Text emphasisText;
        [SerializeField] private string[] emphasisPhrases;
        private static readonly int Updated = Animator.StringToHash("updated");
        private static readonly int Hidden = Animator.StringToHash("hidden");
        private int _lastCombo = 0;
        private Animator _animator;

        #endregion
        
        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
            emphasisText.text = "";
            comboText.text = "";
        }

        public void UpdateCombo(int amount)
        {
            if (amount > 1)
            {
                _animator.SetTrigger(Updated);
                comboText.text = amount.ToString();
                _lastCombo = amount;
            }
            else if (_lastCombo > 0)
            {
                _animator.SetTrigger(Hidden);
                _lastCombo = 0;
            }
            SetEmphasisText();
        }

        private void SetEmphasisText()
        {
            if (_lastCombo == 0)
            {
                return;
            }

            var phrase = emphasisPhrases[Mathf.Min(_lastCombo / 5, emphasisPhrases.Length - 1)];
            emphasisText.text = phrase;
        }
    }
}
