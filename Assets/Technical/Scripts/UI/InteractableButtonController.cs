﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    /// <summary>
    /// [WIP] uses DOTween to perform button animations
    /// </summary>
    public class InteractableButtonController : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler,
        IPointerExitHandler, IPointerUpHandler, IPointerClickHandler
    {

        [SerializeField] private Sprite spriteOnClick;
        private Sprite _sprite;
        [SerializeField]
        private UnityEvent onPointerDown;
        [SerializeField]
        private UnityEvent onPointerUp;
        [SerializeField]
        private UnityEvent onPointerEnter;
        [SerializeField]
        private UnityEvent onPointerExit;
        [SerializeField]
        private UnityEvent onPointerClick;
        
        private Vector3 _originalScale;
        private Image _image;
        
        private void Awake()
        {
            _originalScale = transform.localScale;
            _image = GetComponent<Image>();
            if (_image)
            {
                _sprite = _image.sprite;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            onPointerDown?.Invoke();
            transform.DOScale(_originalScale * 1.1f, 0.1f)
                .SetEase(Ease.InOutQuart);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            onPointerUp?.Invoke();
            onPointerDown?.Invoke();
            transform.DOScale(_originalScale, 0.1f)
                .SetEase(Ease.InOutQuart);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            onPointerEnter?.Invoke();
            transform.DOScale(_originalScale * 1.05f, 0.1f)
                .SetEase(Ease.InOutQuart);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onPointerExit?.Invoke();
            transform.DOScale(_originalScale, 0.1f)
                .SetEase(Ease.InOutQuart);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            onPointerClick?.Invoke();
            if (_image && spriteOnClick)
            {
                _image.sprite = _image.sprite == _sprite ? spriteOnClick : _sprite;
            }
        }
    }
}