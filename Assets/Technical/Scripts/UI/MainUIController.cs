﻿using Technical.Scripts.Singletons;
using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    /// <summary>
    /// Handles main UI
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class MainUIController : MonoBehaviour
    {
        #region Variables

        [SerializeField] private Text recordScoreBomb;
        [SerializeField] private Text recordScoreFreeze;
        [SerializeField] private Button playButton;
        [SerializeField] private Sprite playButtonBlue;
        [SerializeField] private Sprite playButtonRed;

        private Animator _animator;

        private bool playWithBombs = false;
        private static readonly int BombSelected = Animator.StringToHash("bombs_selected");
        private static readonly int FreezeSelected = Animator.StringToHash("freeze_selected");

        #endregion

        #region Unity lifecycle

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        private void Start()
        {
            recordScoreBomb.text = GameController.playerData.scoreRecordBomb.ToString();
            recordScoreFreeze.text = GameController.playerData.scoreRecordFreeze.ToString();
            playButton.interactable = false;
        }

        #endregion

        #region Class methods

        public void SelectGameMode(bool withBombs)
        {
            playButton.interactable = true;
            playButton.GetComponent<Image>().sprite = withBombs ? playButtonRed : playButtonBlue;
            playWithBombs = withBombs;
            _animator.SetBool(BombSelected, playWithBombs);
            _animator.SetBool(FreezeSelected, !playWithBombs);
        }

        public void PlayNewGame()
        {
            GameController.StartGame(playWithBombs);
        }

        #endregion
    }
}