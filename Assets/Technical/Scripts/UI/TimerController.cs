﻿using System;
using System.Timers;
using Technical.Scripts.Singletons;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.UI
{
    [Serializable]
    public class TimerEvent : UnityEvent<float> {}
    
    /// <summary>
    /// Handles timer
    /// </summary>
    public class TimerController : MonoBehaviour
    {
        #region Variables

        public UnityAction<float> ONTimerFreeze;
        
        [SerializeField] private TimerEvent onTick;
        [SerializeField] private TimerEvent onTimeExpire;
        [SerializeField] private float timeAmount = 120;
        
        private float _timeRemaining = 0;
        private float _freezeRemaining = 0;

        #endregion

        #region Unity lifecycle

        private void Start()
        {
            _timeRemaining = timeAmount;
        }

        private void Update()
        {
            if (GameController.gamePaused) return;
            if (_freezeRemaining > 0)
            {
                _freezeRemaining = Mathf.Max(0, _freezeRemaining - Time.deltaTime);
                return;
            }

            if(_freezeRemaining == 0)
            {
                ONTimerFreeze?.Invoke(0);
            }
            
            _timeRemaining -= Time.deltaTime;
            onTick?.Invoke(_timeRemaining);
            
            if (_timeRemaining > 0) return;
            
            onTimeExpire?.Invoke(0);
            GameController.EndGame();
            Destroy(gameObject);
        }

        #endregion

        public void Freeze(float amount)
        {
            _freezeRemaining += amount;
            ONTimerFreeze?.Invoke(amount);
        }
    }
}
