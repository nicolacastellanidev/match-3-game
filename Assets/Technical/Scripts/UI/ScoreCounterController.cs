﻿using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    public class ScoreCounterController : MonoBehaviour
    {
        private Text _text;
        private Animator _animator;
        private static readonly int Updated = Animator.StringToHash("updated");

        private void Awake()
        {
            _text = GetComponent<Text>();
            _animator = GetComponent<Animator>();
        }

        public void UpdateText(string amount)
        {
            _text.text = amount;
            if(amount != "0") _animator.SetTrigger(Updated);
        }
    }
}
