﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Singletons
{
    /// <summary>
    /// Handle user score
    /// </summary>
    public class ScoreController : MonoBehaviour
    {
        #region Variables
        
        [SerializeField] public float scorePerTile = 10;
        [SerializeField] public float comboMultiplier = 1.1f;
        [SerializeField] private float comboResetTime = 2f;
        
        public static UnityAction<int> ONScoreUpdate;
        public static UnityAction<int> ONComboUpdate;
        public static float score => instance._currentScore;
        
        private static ScoreController instance;
        private float _currentScore = 0;
        private int _currentCombo = 0;
        private float _updatedScore = 0;
        private int _updatedCombo = 0;
        
        #endregion

        #region Unity lifecycle methods
        
        private void Awake()
        {
            if (!instance)
            {
                instance = this;
            } else if (instance != this)
            {
                Destroy(this);
            }
        }
        
        private void Start()
        {
            ONScoreUpdate?.Invoke((int)instance._currentScore);
            GameController.ONGameEnded += ForceCurrentScore;
        }

        private void OnDestroy()
        {
            instance.StopCoroutine(nameof(UpdateCurrentScore));
            instance.StopCoroutine(nameof(UpdateComboAmount));
            instance.CancelInvoke(nameof(ResetCombo));
            GameController.ONGameEnded -= ForceCurrentScore;
        }
        
        #endregion

        #region Class methods
        
        /// <summary>
        /// Called after a mathc
        /// </summary>
        /// <param name="totalTiles">int</param>
        public static void ComboMatched(int totalTiles)
        {
            var currentScore = instance.scorePerTile * totalTiles;
            instance._currentCombo += totalTiles / 3;
            instance._currentScore += currentScore + instance.scorePerTile * (instance.comboMultiplier * instance._currentCombo - 1);
            
            instance.StopCoroutine(nameof(UpdateCurrentScore));
            instance.StartCoroutine(nameof(UpdateCurrentScore));
            
            // COMBO
            instance.CancelInvoke(nameof(ResetCombo));
            instance.Invoke(nameof(ResetCombo), instance.comboResetTime);
            
            if (instance._currentCombo > 1)
            {
                instance.StopCoroutine(nameof(UpdateComboAmount));
                instance.StartCoroutine(nameof(UpdateComboAmount));
            }
            else
            {
                instance.StopCoroutine(nameof(UpdateComboAmount));
            }
        }

        private void ForceCurrentScore(bool noMatches)
        {
            _updatedScore = _currentScore;
            ONScoreUpdate?.Invoke((int)_updatedScore);
        }

        /// <summary>
        /// Update score and propagate event
        /// </summary>
        private IEnumerator UpdateCurrentScore()
        {
            while (_updatedScore < _currentScore)
            {
                _updatedScore = Mathf.Min(_currentScore, _updatedScore + 1);
                ONScoreUpdate?.Invoke((int)_updatedScore);
                yield return new WaitForSeconds(0.01f);
            }
        }

        /// <summary>
        /// Reset current combo
        /// </summary>
        private void ResetCombo()
        {
            _currentCombo = 0;
            _updatedCombo = 0;
            ONComboUpdate?.Invoke(instance._currentCombo);
        }

        /// <summary>
        /// updates combo counter
        /// </summary>
        private IEnumerator UpdateComboAmount()
        {
            while (_updatedCombo < _currentCombo)
            {
                _updatedCombo = Mathf.Min(_currentCombo + 1, _updatedCombo + 1);
                ONComboUpdate?.Invoke(_updatedCombo);
                yield return new WaitForSeconds(0.25f);
            }
        }
        
        #endregion
    }
}
