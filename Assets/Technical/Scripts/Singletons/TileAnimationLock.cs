﻿using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Singletons
{
    /// <summary>
    /// Handles lock on tile animations
    /// </summary>
    public class TileAnimationLock : MonoBehaviour
    {
        public static UnityAction lockFree;
        public static UnityAction lockNotFree;
        public static bool Locked => instance.lockStack > 0;

        private static TileAnimationLock instance;
        private int lockStack = 0;

        private void Awake()
        {
            if (!instance)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(this);
            }
        }

        /// <summary>
        /// Increase lock stack
        /// </summary>
        public static void Lock()
        {
            instance.lockStack++;
            if (instance.lockStack > 0)
            {
                lockNotFree?.Invoke();
            }
        }
        
        /// <summary>
        /// Decreases lock stack
        /// </summary>
        public static void Unlock()
        {
            if (instance.lockStack == 0) return;
            instance.lockStack--;
            if (instance.lockStack <= 0)
            {
                lockFree?.Invoke();
            }
        }
    }
}