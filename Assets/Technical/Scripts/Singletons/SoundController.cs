﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Technical.Scripts.Singletons
{
    /// <summary>
    /// Handles game sounds
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class SoundController : MonoBehaviour
    {
        #region variables

        private static SoundController instance;
        [SerializeField] private AudioSource mainAudio;
        [SerializeField] private AudioSource gameAudio;
        [Range(0, 1f)][SerializeField] private float maxVolume = 0.75f;

        #endregion

        #region Unity lifecycle methods

        private void Awake()
        {
            if (instance != null && instance != this)
                Destroy(gameObject);
            else
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
            PlayMainMusic();
        }

        #endregion

        #region Class methods

        /// <summary>
        /// Plays music by scene
        /// </summary>
        public static void PlayMusicByScene()
        {
            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                PlayMainMusic();
            }
            else
            {
                PlayInGameMusic();
            }
        }
    
        /// <summary>
        /// Plays main scene music
        /// </summary>
        public static void PlayMainMusic()
        {
            instance.StopAllCoroutines();
            instance.StartCoroutine(FadeAudio(instance.mainAudio, instance.maxVolume));
            instance.StartCoroutine(FadeAudio(instance.gameAudio, 0));
        }

        /// <summary>
        /// Plays in game music
        /// </summary>
        /// <param name="restart">bool</param>
        public static void PlayInGameMusic(bool restart = false)
        {
            instance.StopAllCoroutines();
            if (restart)
            {
                instance.gameAudio.volume = 0f;
                instance.gameAudio.Stop();
                instance.gameAudio.Play();
            }
            instance.StartCoroutine(FadeAudio(instance.gameAudio, instance.maxVolume));
            instance.StartCoroutine(FadeAudio(instance.mainAudio, 0));
        }

        /// <summary>
        /// Fade audio from current volume to target
        /// </summary>
        /// <param name="target">AudioSource</param>
        /// <param name="targetVolume">float</param>
        /// <returns></returns>
        private static IEnumerator FadeAudio(AudioSource target, float targetVolume)
        {
            if (Math.Abs(target.volume - targetVolume) < 0.01f)
            {
                yield return null;
            }
            else
            {
                if(!target.isPlaying) target.Play();
                while (Math.Abs(target.volume - targetVolume) > 0.01f)
                {
                    target.volume = Mathf.Lerp(target.volume, targetVolume, 0.1f);
                    yield return new WaitForSeconds(0.1f);
                }

                target.volume = targetVolume;
                if(targetVolume == 0) target.Stop();   
            }
        }

        #endregion
    }
}
