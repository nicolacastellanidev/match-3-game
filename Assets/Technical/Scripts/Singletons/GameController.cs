﻿using Technical.Scripts.API;
using Technical.Scripts.Models;
using Technical.Scripts.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Technical.Scripts.Singletons
{
    /// <summary>
    /// Handles game logic
    /// </summary>
    public class GameController : MonoBehaviour
    {
        #region Variables
        public static GameController instance;
        public static bool useBombs = false;
        public static bool gamePaused = false;
        public static UnityAction<bool> ONGameEnded;
        public static UnityAction ONStopMatching;
        public static UnityAction ONSceneLoaded;
        public static PlayerData playerData;
        
        private static TimerController _timer;
        public static TimerController timer
        {
            get
            {
                if (!_timer)
                {
                    _timer = FindObjectOfType<TimerController>();
                }

                return _timer;
            }
        }

        #endregion
        
        #region Unity lifecycle
        private void Awake()
        {
            if (instance != null && instance != this)
                Destroy(gameObject);
            else
            {
                instance = this;
                DontDestroyOnLoad(this);
            }

            playerData = PlayerDataAPI.LoadData();
        }

        private void Start()
        {
            PlayMusicByScene();
#if UNITY_EDITOR
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                ONSceneLoaded?.Invoke();
            }
#endif
        }
        #endregion
        #region Static methods
        
        /// <summary>
        /// Start a new game using bombs or not
        /// </summary>
        /// <param name="withBombs">bool</param>
        public static void StartGame(bool withBombs)
        {
            useBombs = withBombs;
            var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            LoadingScreenController.Show(currentSceneIndex == 0 ? 1 : 0);
        }
        
        /// <summary>
        /// Ends game and save score based on game mode
        /// </summary>
        /// <param name="noMatches">bool</param>
        public static void EndGame(bool noMatches = false)
        {
            TogglePause(true);
            var score = ScoreController.score;
            if (useBombs)
            {
                if (score > playerData.scoreRecordBomb)
                {
                    playerData.scoreRecordBomb = score;
                    PlayerDataAPI.SaveData(playerData);
                }
            }
            else
            {
                if (score > playerData.scoreRecordFreeze)
                {
                    playerData.scoreRecordFreeze = score;
                    PlayerDataAPI.SaveData(playerData);
                }
            }

            ONGameEnded?.Invoke(noMatches);
        }

        /// <summary>
        /// Called when load scene async ends
        /// </summary>
        public static void AfterSceneLoaded()
        {
            TogglePause(false);
            instance.PlayMusicByScene();
            ONSceneLoaded?.Invoke();
        }

        /// <summary>
        /// Returns to main scene
        /// </summary>
        public static void BackToMainScene()
        {
            SoundController.PlayMainMusic();
            LoadingScreenController.Show(0);
            ONStopMatching?.Invoke();
            TogglePause(false);
        }

        /// <summary>
        /// Starts a new game
        /// </summary>
        public static void ReloadGameScene()
        {
            SoundController.PlayInGameMusic(true);
            LoadingScreenController.Show(1);
            ONStopMatching?.Invoke();
            TogglePause(false);
        }

        /// <summary>
        /// Pause the game
        /// </summary>
        /// <param name="pause">bool</param>
        public static void TogglePause(bool pause)
        {
            gamePaused = pause;
            Time.timeScale = gamePaused ? 0 : 1;
        }

        /// <summary>
        /// Call timer, and freeze the time
        /// </summary>
        /// <param name="amount"></param>
        public static void FreezeTime(float amount)
        {
            timer.Freeze(amount);
        }

        /// <summary>
        /// Call SoundController PlayMusicByScene
        /// </summary>
        private void PlayMusicByScene()
        {
            SoundController.PlayMusicByScene();
        }
        #endregion
    }
}