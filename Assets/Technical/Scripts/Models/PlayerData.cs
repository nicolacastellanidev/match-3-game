﻿namespace Technical.Scripts.Models
{
    /// <summary>
    /// Class used to store player data
    /// </summary>
    public class PlayerData
    {
        public float scoreRecordBomb = 0;
        public float scoreRecordFreeze = 0;
    }
}