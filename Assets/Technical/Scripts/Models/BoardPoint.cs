﻿namespace Technical.Scripts.Models
{
    /// <summary>
    /// Defines a point on board
    /// </summary>
    public class BoardPoint
    {
        public readonly int row;
        public readonly int column;

        public BoardPoint(int row, int column)
        {
            this.row = row;
            this.column = column;
        }

        public bool Equals(BoardPoint other)
        {
            return row == other.row && column == other.column;
        }
    }
}