﻿using UnityEngine;

namespace Technical.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "BoardTileData", menuName = "ScriptableObjects/TileData", order = 1)]
    public class BoardTileData : ScriptableObject
    {
        [Space(10)] [Header("Graphics")] public Sprite sprite;
        public Sprite spriteOnSelection;
        [Space(10)] [Header("Sounds")] public AudioClip soundOnPop;
        public AudioClip soundOnShake;
        public AudioClip soundOnSelect;

        [Space(10)]
        [Header("Other settings")]
        [Tooltip("Amount of influence if current seed is special (freeze time for example)")]
        public float amount = 0;

        [HideInInspector] public int seed;
    }
}