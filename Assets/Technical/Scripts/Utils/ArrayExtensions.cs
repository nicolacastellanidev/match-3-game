﻿using System;

namespace Technical.Scripts
{
    internal static class ArrayExtensions
    {
        private static readonly Random Rnd = new Random();
        /// <summary>
        /// Randomizes array
        /// </summary>
        public static T[] Shuffle<T>(this T[] array)
        {
            for (var i = array.Length; i > 1; i--)
            {
                // Pick random element to swap.
                var j = Rnd.Next(i);
                // Swap.
                var tmp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = tmp;
            }
            return array;
        }
    }
}