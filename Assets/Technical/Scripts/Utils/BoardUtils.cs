﻿using System.Collections.Generic;
using Technical.Scripts.Models;

namespace Technical.Scripts
{
    public static class BoardUtils
    {
        /// <summary>
        /// Removes duplicated elements on board
        /// </summary>
        /// <param name="what">IEnumerable<BoardPoint></param>
        /// <returns>List<BoardPoint></returns>
        public static List<BoardPoint> Distinct(IEnumerable<BoardPoint> what)
        {
            var result = new List<BoardPoint>();

            foreach (var item in what)
            {
                if (result.Exists(ints => ints.Equals(item))) continue;
                result.Add(item);
            }

            return result;
        }
    }
}