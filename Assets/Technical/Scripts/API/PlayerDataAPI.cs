﻿using System;
using System.IO;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.API
{
    /// <summary>
    /// Saves and load Player Data
    /// </summary>
    public static class PlayerDataAPI
    {
        public static PlayerData TestData = new PlayerData();
        
        /// <summary>
        /// Load data from persistent data path
        /// </summary>
        /// <param name="testMode">bool</param>
        /// <returns>PlayerData</returns>
        public static PlayerData LoadData(bool testMode = false)
        {
            if (testMode)
            {   
                return TestData;
            }
            var filepath = Application.persistentDataPath + "/player_data.json";
            var readJson = "";
            try
            {
                using (var sr = new StreamReader(filepath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        readJson += line;
                    }
                }
            }
            catch (FileNotFoundException)
            {
                return new PlayerData();
            }
            return readJson != "" ? JsonUtility.FromJson<PlayerData>(readJson) : new PlayerData();
        }
        
        /// <summary>
        /// Saves data to persistent path;
        /// </summary>
        /// <param name="data">PlayerData</param>
        /// <param name="test">bool</param>
        /// <returns>bool</returns>
        public static bool SaveData(PlayerData data, bool test = false)
        {
            var filepath = Application.persistentDataPath + "/player_data.json";
            try
            {
                var saveJson = JsonUtility.ToJson(data);
                using (var sw = new StreamWriter(filepath))
                {
                    if (!test)
                    {
                        sw.WriteLine(saveJson);
                    }
                    else
                    {
                        Debug.Log($"Writing data to JSON: {saveJson}");
                        TestData = data;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"An error has occurred while saving data: {e}");
                return false;
            }
            return true;
        }
    }
}