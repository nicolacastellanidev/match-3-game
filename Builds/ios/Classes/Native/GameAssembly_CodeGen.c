﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Technical.Scripts.ScoreCounterController::Awake()
extern void ScoreCounterController_Awake_m3B17D22850C963EF93E0D3CCD4B7347DA645E385 (void);
// 0x00000002 System.Void Technical.Scripts.ScoreCounterController::UpdateText(System.String)
extern void ScoreCounterController_UpdateText_mB49C90C8445F77A3ECDBE49931F48F15E8A07480 (void);
// 0x00000003 System.Void Technical.Scripts.ScoreCounterController::.ctor()
extern void ScoreCounterController__ctor_mC0994BD213E8F80B38C72F611B9129E55336B006 (void);
// 0x00000004 System.Void Technical.Scripts.ScoreCounterController::.cctor()
extern void ScoreCounterController__cctor_mB9AF772DC0B169BC8299C3B0AAF35B53FDA9775A (void);
// 0x00000005 T[] Technical.Scripts.ArrayExtensions::Shuffle(T[])
// 0x00000006 System.Void Technical.Scripts.ArrayExtensions::.cctor()
extern void ArrayExtensions__cctor_m390EC716F5FC7245214A35EF87591D9F6488C78E (void);
// 0x00000007 System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.BoardUtils::Distinct(System.Collections.Generic.IEnumerable`1<Technical.Scripts.Models.BoardPoint>)
extern void BoardUtils_Distinct_m86CA8B299979691D5ECCCFE46E3139CCDA606F72 (void);
// 0x00000008 System.Void Technical.Scripts.CameraResolutionHandler::Awake()
extern void CameraResolutionHandler_Awake_mE76810C130CC2ACCF53F3C779C27E52DB2BAD7D3 (void);
// 0x00000009 System.Void Technical.Scripts.CameraResolutionHandler::OnPreCull()
extern void CameraResolutionHandler_OnPreCull_m1F34C250A47384D63A32F3214CA9421D02F7F2BA (void);
// 0x0000000A System.Void Technical.Scripts.CameraResolutionHandler::Start()
extern void CameraResolutionHandler_Start_m5CBE0D7630C01C73CA742BA6F890656C755FFE64 (void);
// 0x0000000B System.Void Technical.Scripts.CameraResolutionHandler::RescaleCamera()
extern void CameraResolutionHandler_RescaleCamera_m27551C9A4CDF7809C6298775A5BB0A52DF02DD59 (void);
// 0x0000000C System.Void Technical.Scripts.CameraResolutionHandler::.ctor()
extern void CameraResolutionHandler__ctor_m21F20ACB7622085517FED78D6435DEC851152437 (void);
// 0x0000000D System.Void Technical.Scripts.UI.ComboCounterController::Awake()
extern void ComboCounterController_Awake_m282B243EA414E6D833CCB72C8F6DE04F63866C02 (void);
// 0x0000000E System.Void Technical.Scripts.UI.ComboCounterController::UpdateCombo(System.Int32)
extern void ComboCounterController_UpdateCombo_m6C9C0D15802D462F8AB45510DB2CB03757E77D81 (void);
// 0x0000000F System.Void Technical.Scripts.UI.ComboCounterController::SetEmphasisText()
extern void ComboCounterController_SetEmphasisText_mCFBAEB1D4BFFC8B3B65AECB41162FC446CBE569F (void);
// 0x00000010 System.Void Technical.Scripts.UI.ComboCounterController::.ctor()
extern void ComboCounterController__ctor_m678226EF12D899572DA001AEF0B76C2709192BF5 (void);
// 0x00000011 System.Void Technical.Scripts.UI.ComboCounterController::.cctor()
extern void ComboCounterController__cctor_mE816BF2158BB920F00CEB41BA8BD7E2E8B87FD28 (void);
// 0x00000012 System.Void Technical.Scripts.UI.GameUIController::Start()
extern void GameUIController_Start_m4AD9B043028771D06F0829618B94FF957033C17A (void);
// 0x00000013 System.Void Technical.Scripts.UI.GameUIController::OnTimerFreeze(System.Single)
extern void GameUIController_OnTimerFreeze_m28C516D130823C61FBE2C1F74AEFA23BC63EC3FF (void);
// 0x00000014 System.Void Technical.Scripts.UI.GameUIController::OnDestroy()
extern void GameUIController_OnDestroy_m76DB9CE39C901F8E06E660C2447E39553D819E5F (void);
// 0x00000015 System.Void Technical.Scripts.UI.GameUIController::TogglePauseMenu()
extern void GameUIController_TogglePauseMenu_m45D56E80C69A62DA86616D9640C17C49BC08B0ED (void);
// 0x00000016 System.Void Technical.Scripts.UI.GameUIController::UpdateTimerText(System.Single)
extern void GameUIController_UpdateTimerText_m134EBBC46FC2AAF12939D14DA94839A231381028 (void);
// 0x00000017 System.Void Technical.Scripts.UI.GameUIController::ShowEndGameUI(System.Boolean)
extern void GameUIController_ShowEndGameUI_m9B0128D10E4E6BA1702BBA60D288F8FAEDD7A26B (void);
// 0x00000018 System.Void Technical.Scripts.UI.GameUIController::BackToHomePage()
extern void GameUIController_BackToHomePage_m49EF78AA1EF29F306EA054689EA42F00ED42F20F (void);
// 0x00000019 System.Void Technical.Scripts.UI.GameUIController::RestartGame()
extern void GameUIController_RestartGame_m42D7F371181FE0DB4F8DF0856EE150AD32562CEB (void);
// 0x0000001A System.Void Technical.Scripts.UI.GameUIController::UpdateScore(System.Int32)
extern void GameUIController_UpdateScore_m4ED5CDB777F0BFDA52B0825F55CDBA357AB47CF9 (void);
// 0x0000001B System.Void Technical.Scripts.UI.GameUIController::UpdateCombo(System.Int32)
extern void GameUIController_UpdateCombo_mC48E0E9D9C5DD891D738D5E64DA3A55A5F9F2539 (void);
// 0x0000001C System.Void Technical.Scripts.UI.GameUIController::.ctor()
extern void GameUIController__ctor_m210C91EA10F399BB108C74E92A2349DE0DE7CCB5 (void);
// 0x0000001D System.Void Technical.Scripts.UI.InteractableButtonController::Awake()
extern void InteractableButtonController_Awake_mA02B7468A7E16ADD8F5760985BB01E65BF2CCAD1 (void);
// 0x0000001E System.Void Technical.Scripts.UI.InteractableButtonController::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void InteractableButtonController_OnPointerDown_m56ADAB22EE21D5454F938FAA9A223B51ED2CE61A (void);
// 0x0000001F System.Void Technical.Scripts.UI.InteractableButtonController::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void InteractableButtonController_OnPointerUp_m8C93F6CD461C18440F0D0407DFA6FE650CB78A1A (void);
// 0x00000020 System.Void Technical.Scripts.UI.InteractableButtonController::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void InteractableButtonController_OnPointerEnter_m5BBEBF8EFB4B43E48E77688500C6B675EA51847C (void);
// 0x00000021 System.Void Technical.Scripts.UI.InteractableButtonController::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void InteractableButtonController_OnPointerExit_m6BF56BC26672242B477992958A8F6BC64D11C81F (void);
// 0x00000022 System.Void Technical.Scripts.UI.InteractableButtonController::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void InteractableButtonController_OnPointerClick_m209AEB211650A64C624B303623162DF220D4CE10 (void);
// 0x00000023 System.Void Technical.Scripts.UI.InteractableButtonController::.ctor()
extern void InteractableButtonController__ctor_mCCC06E85F217757E3E17255683093E7E3C2072B0 (void);
// 0x00000024 System.Void Technical.Scripts.UI.LoadingScreenController::Awake()
extern void LoadingScreenController_Awake_m5E3E7937AA30A0F28DB59A66F3AAA540C47395BE (void);
// 0x00000025 System.Void Technical.Scripts.UI.LoadingScreenController::Show(System.Int32)
extern void LoadingScreenController_Show_m7F322D9C01CBF5AE84A60DE62B0AE20A6B2578AE (void);
// 0x00000026 System.Void Technical.Scripts.UI.LoadingScreenController::Hide()
extern void LoadingScreenController_Hide_m394D863FFBDE079BA649920421DA2A710AA7CCF4 (void);
// 0x00000027 System.Collections.IEnumerator Technical.Scripts.UI.LoadingScreenController::LoadSceneAsync(System.Int32)
extern void LoadingScreenController_LoadSceneAsync_m0D1A4B3C8755505B22D5268F5D95D79497AEEECF (void);
// 0x00000028 System.Void Technical.Scripts.UI.LoadingScreenController::.ctor()
extern void LoadingScreenController__ctor_mF4D53622A4345FAED66C3BA97E24A41E56EAB407 (void);
// 0x00000029 System.Void Technical.Scripts.UI.LoadingScreenController::.cctor()
extern void LoadingScreenController__cctor_m595EF24449F9922460763A06C4C0D21097D21506 (void);
// 0x0000002A System.Void Technical.Scripts.UI.MainUIController::Awake()
extern void MainUIController_Awake_m716197C8BA8F58BEB95A7BE85FC0BA99D864ADF9 (void);
// 0x0000002B System.Void Technical.Scripts.UI.MainUIController::Start()
extern void MainUIController_Start_mDA7E37D8CBE3F01B259A6A2357F55EB9FFA17E85 (void);
// 0x0000002C System.Void Technical.Scripts.UI.MainUIController::SelectGameMode(System.Boolean)
extern void MainUIController_SelectGameMode_mE0E537BF8D138A377303715E2CEC10925B34BA5D (void);
// 0x0000002D System.Void Technical.Scripts.UI.MainUIController::PlayNewGame()
extern void MainUIController_PlayNewGame_m898A4EEB07E33D0F869323FDA9727AFA40AF5585 (void);
// 0x0000002E System.Void Technical.Scripts.UI.MainUIController::.ctor()
extern void MainUIController__ctor_m71E0B2A5167F8FD6D2BB6FAA9247D785D1534E50 (void);
// 0x0000002F System.Void Technical.Scripts.UI.MainUIController::.cctor()
extern void MainUIController__cctor_m93855953C314C8C7563F1E55836510C910DABE07 (void);
// 0x00000030 System.Void Technical.Scripts.UI.TimerEvent::.ctor()
extern void TimerEvent__ctor_m68CC9A30E7499EFCC4E492F9C22651D8A6CBDF79 (void);
// 0x00000031 System.Void Technical.Scripts.UI.TimerController::Start()
extern void TimerController_Start_m02335138F6469881B1ED42BBE76738E652AE1E2C (void);
// 0x00000032 System.Void Technical.Scripts.UI.TimerController::Update()
extern void TimerController_Update_m8787EF5EB60AA8C0A81BE1B8AAC82871FE611757 (void);
// 0x00000033 System.Void Technical.Scripts.UI.TimerController::Freeze(System.Single)
extern void TimerController_Freeze_m5B5FF2F202D719E05C20E65987AA89D8778D01A5 (void);
// 0x00000034 System.Void Technical.Scripts.UI.TimerController::.ctor()
extern void TimerController__ctor_m0FBD89E8529AA430F332A84E70941177BE53A8ED (void);
// 0x00000035 Technical.Scripts.UI.TimerController Technical.Scripts.Singletons.GameController::get_timer()
extern void GameController_get_timer_m89A2ED7BB0CFA358EA132989FD916C2631DCAD32 (void);
// 0x00000036 System.Void Technical.Scripts.Singletons.GameController::Awake()
extern void GameController_Awake_mE3FA01A55D6E645F524D18C9CF140CA44AF805B5 (void);
// 0x00000037 System.Void Technical.Scripts.Singletons.GameController::Start()
extern void GameController_Start_mECC7E53B35E7AEF76CFFDD084DF567DCCF9D44A8 (void);
// 0x00000038 System.Void Technical.Scripts.Singletons.GameController::StartGame(System.Boolean)
extern void GameController_StartGame_mBE99B45447C9C25F3F08ED301775AB3A7D839E8B (void);
// 0x00000039 System.Void Technical.Scripts.Singletons.GameController::EndGame(System.Boolean)
extern void GameController_EndGame_mFB8ACA7262127E44A0BFDD65A1819A94FA790399 (void);
// 0x0000003A System.Void Technical.Scripts.Singletons.GameController::AfterSceneLoaded()
extern void GameController_AfterSceneLoaded_m75F4FD03A3295D5B1D5F95C2D6644640C1FD878C (void);
// 0x0000003B System.Void Technical.Scripts.Singletons.GameController::BackToMainScene()
extern void GameController_BackToMainScene_m4829558F5A3EB76D078DA5672E9483E3E8B85B43 (void);
// 0x0000003C System.Void Technical.Scripts.Singletons.GameController::ReloadGameScene()
extern void GameController_ReloadGameScene_m0D97887140E059EEBAA0BA6259E25EDAB2D561B0 (void);
// 0x0000003D System.Void Technical.Scripts.Singletons.GameController::TogglePause(System.Boolean)
extern void GameController_TogglePause_mB470177B9E046E514437F51666C2139119BBE460 (void);
// 0x0000003E System.Void Technical.Scripts.Singletons.GameController::FreezeTime(System.Single)
extern void GameController_FreezeTime_m2F35B57C99D146CB199255CFC788F1AB32D56372 (void);
// 0x0000003F System.Void Technical.Scripts.Singletons.GameController::PlayMusicByScene()
extern void GameController_PlayMusicByScene_m4A8B1E07365FE960FAF499CC714C0D66934ADB79 (void);
// 0x00000040 System.Void Technical.Scripts.Singletons.GameController::.ctor()
extern void GameController__ctor_mE04C9DC0F13365C53B13332D0847E8655C4C53E6 (void);
// 0x00000041 System.Void Technical.Scripts.Singletons.GameController::.cctor()
extern void GameController__cctor_mD80FDAAD0770B4735E6B688A5470C6027E223E36 (void);
// 0x00000042 System.Single Technical.Scripts.Singletons.ScoreController::get_score()
extern void ScoreController_get_score_mB3F8668DAB993EEC28D26AEAC267238FF7B69BDA (void);
// 0x00000043 System.Void Technical.Scripts.Singletons.ScoreController::Awake()
extern void ScoreController_Awake_m6E14058A26132A5D9B3A7C64733624DDEC120A33 (void);
// 0x00000044 System.Void Technical.Scripts.Singletons.ScoreController::Start()
extern void ScoreController_Start_mC7FCAB4251B3671E2DF56456862EA48145463FCF (void);
// 0x00000045 System.Void Technical.Scripts.Singletons.ScoreController::OnDestroy()
extern void ScoreController_OnDestroy_m033A4C3B11382DEE724B60E7241F346603FF09DC (void);
// 0x00000046 System.Void Technical.Scripts.Singletons.ScoreController::ComboMatched(System.Int32)
extern void ScoreController_ComboMatched_m31E9F24C201D4C66AB8557003B134110E0624464 (void);
// 0x00000047 System.Collections.IEnumerator Technical.Scripts.Singletons.ScoreController::UpdateCurrentScore()
extern void ScoreController_UpdateCurrentScore_m140BA6C8BB7CDF26281435EAD2E6C78A937ADA85 (void);
// 0x00000048 System.Void Technical.Scripts.Singletons.ScoreController::ResetCombo()
extern void ScoreController_ResetCombo_mE1CCA142E954B02A6A5A6DAFCB2F3EB2068D4985 (void);
// 0x00000049 System.Collections.IEnumerator Technical.Scripts.Singletons.ScoreController::UpdateComboAmount()
extern void ScoreController_UpdateComboAmount_mCAD7A17AED5EEFD63E2F386F6C30BCB5B237AF6C (void);
// 0x0000004A System.Void Technical.Scripts.Singletons.ScoreController::.ctor()
extern void ScoreController__ctor_m2A9D1CB9D9D26076F614794377B245C0AFE09312 (void);
// 0x0000004B System.Void Technical.Scripts.Singletons.SoundController::Awake()
extern void SoundController_Awake_m91F5439268082D6DD97777E1248AA65192349654 (void);
// 0x0000004C System.Void Technical.Scripts.Singletons.SoundController::PlayMusicByScene()
extern void SoundController_PlayMusicByScene_mACA9E8E80C1990EB3B94235DEE2DC29E2232E8BB (void);
// 0x0000004D System.Void Technical.Scripts.Singletons.SoundController::PlayMainMusic()
extern void SoundController_PlayMainMusic_mEBFD0D918A78011C379039835AFE412C28D14EC5 (void);
// 0x0000004E System.Void Technical.Scripts.Singletons.SoundController::PlayInGameMusic(System.Boolean)
extern void SoundController_PlayInGameMusic_m75E34C3C77D7827A39DE445015DA91C3B1E24304 (void);
// 0x0000004F System.Collections.IEnumerator Technical.Scripts.Singletons.SoundController::FadeAudio(UnityEngine.AudioSource,System.Single)
extern void SoundController_FadeAudio_mCE7BA2DF89C56A5CCFD02A3B401A6B660CEBE669 (void);
// 0x00000050 System.Void Technical.Scripts.Singletons.SoundController::.ctor()
extern void SoundController__ctor_m4C13E014B9D4687951E2D29D010E630482A5556E (void);
// 0x00000051 System.Boolean Technical.Scripts.Singletons.TileAnimationLock::get_Locked()
extern void TileAnimationLock_get_Locked_m2A543847ACA1A333501A7A2A19261C2146EA08A7 (void);
// 0x00000052 System.Void Technical.Scripts.Singletons.TileAnimationLock::Awake()
extern void TileAnimationLock_Awake_m3701D8E337419D2B92FB5372BA58992DC888EEDC (void);
// 0x00000053 System.Void Technical.Scripts.Singletons.TileAnimationLock::Lock()
extern void TileAnimationLock_Lock_mD47DA611EC236E75ACE2E793E80ED9029AA19617 (void);
// 0x00000054 System.Void Technical.Scripts.Singletons.TileAnimationLock::Unlock()
extern void TileAnimationLock_Unlock_m99CA531047BB60F6F062EEF8B288A35350111409 (void);
// 0x00000055 System.Void Technical.Scripts.Singletons.TileAnimationLock::.ctor()
extern void TileAnimationLock__ctor_m8F76B8DDE19781DEDB521A4BB4C99F98A6BB2678 (void);
// 0x00000056 System.Void Technical.Scripts.ScriptableObjects.BoardTileData::.ctor()
extern void BoardTileData__ctor_mE26CD074659A96D789636F0601CBC59787ACEEB5 (void);
// 0x00000057 System.Void Technical.Scripts.Models.BoardPoint::.ctor(System.Int32,System.Int32)
extern void BoardPoint__ctor_mB8E9490AEE1BFC9D1E90C7F57388A2993DC9B4DF (void);
// 0x00000058 System.Boolean Technical.Scripts.Models.BoardPoint::Equals(Technical.Scripts.Models.BoardPoint)
extern void BoardPoint_Equals_m5907168A1A3587A55E4E3AEB019C00EFC9D296F8 (void);
// 0x00000059 System.Void Technical.Scripts.Models.PlayerData::.ctor()
extern void PlayerData__ctor_mA6134AE624F63343075609F6DD2636AA72148CFC (void);
// 0x0000005A System.Void Technical.Scripts.Board.BoardController::Start()
extern void BoardController_Start_m153FF4C86BC4C96435F35AC0A3C70C6751656960 (void);
// 0x0000005B System.Void Technical.Scripts.Board.BoardController::OnDestroy()
extern void BoardController_OnDestroy_mA0F24A072AD362EB217E3918AC0406DC106F27F2 (void);
// 0x0000005C System.Void Technical.Scripts.Board.BoardController::GenerateBoard()
extern void BoardController_GenerateBoard_mAF6E88394B067F7A4914ADA9A9064C1C803ED669 (void);
// 0x0000005D System.Void Technical.Scripts.Board.BoardController::GenerateTiles()
extern void BoardController_GenerateTiles_mD1221F07D6CFA52B57D89E85B3CE8ADBABAC0914 (void);
// 0x0000005E System.Void Technical.Scripts.Board.BoardController::TileClick(Technical.Scripts.Board.TileController)
extern void BoardController_TileClick_m4328556BD27A91DEA516D9F282984A1D39BB0D96 (void);
// 0x0000005F System.Void Technical.Scripts.Board.BoardController::CheckMatch(Technical.Scripts.Board.TileController)
extern void BoardController_CheckMatch_mF85D75AB6A325B16FE9C12B89956A9AD9329214F (void);
// 0x00000060 System.Void Technical.Scripts.Board.BoardController::MatchAll()
extern void BoardController_MatchAll_m9EF10DC76D7695C9C76361A72720AE8CEF8077F2 (void);
// 0x00000061 System.Void Technical.Scripts.Board.BoardController::RefillAndMatch()
extern void BoardController_RefillAndMatch_m541E711B66BEDE2519372BE72C43DC99A20E5C20 (void);
// 0x00000062 System.Void Technical.Scripts.Board.BoardController::PerformMatch(System.Collections.Generic.IEnumerable`1<Technical.Scripts.Models.BoardPoint>)
extern void BoardController_PerformMatch_mDAFDCE8AC8B0E76C083C54F8D30445D4B20083ED (void);
// 0x00000063 System.Void Technical.Scripts.Board.BoardController::PerformRefill()
extern void BoardController_PerformRefill_mC839EFC57D485EC0A3AA6C7E47E867A784EA41ED (void);
// 0x00000064 Technical.Scripts.ScriptableObjects.BoardTileData Technical.Scripts.Board.BoardController::GetDataForSeed(System.Int32)
extern void BoardController_GetDataForSeed_m04F781F158E6E73863569DA7921EAB5C5B3BE50B (void);
// 0x00000065 System.Void Technical.Scripts.Board.BoardController::ClearHighlightedTiles()
extern void BoardController_ClearHighlightedTiles_m2B1BCD4EE0CBC332566D684958E8F2AB918B6E4E (void);
// 0x00000066 System.Void Technical.Scripts.Board.BoardController::StopAll()
extern void BoardController_StopAll_m8957B5E80928A2F48D14DE44789350F0DCCF0C9C (void);
// 0x00000067 System.Void Technical.Scripts.Board.BoardController::.ctor()
extern void BoardController__ctor_mF3DBF37CA12DA3DFCD0CA1E876E5D792D2104914 (void);
// 0x00000068 System.Int32[0...,0...] Technical.Scripts.Board.BoardGenerator::CreateMatrix(System.Int32,System.Int32,System.Int32[],System.Single)
extern void BoardGenerator_CreateMatrix_m2E94DFBCCA79D588A5FCB8B75F4E735BC63070F7 (void);
// 0x00000069 System.Int32 Technical.Scripts.Board.BoardGenerator::GetRandomSeedsByRow(System.Collections.Generic.IEnumerable`1<System.Int32>,System.Collections.Generic.IList`1<System.Int32>,System.Int32,System.Single)
extern void BoardGenerator_GetRandomSeedsByRow_mC4473F36CDA41733A69B7EB51416A4AF55EF298E (void);
// 0x0000006A System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardMatcher::MatchAll(System.Int32[0...,0...],Technical.Scripts.Models.BoardPoint,Technical.Scripts.Models.BoardPoint,System.Boolean)
extern void BoardMatcher_MatchAll_m767E976934D0C15FCE28C92DE7C9598DBD96D22F (void);
// 0x0000006B System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardMatcher::TestSwipeMatch(System.Int32[0...,0...],Technical.Scripts.Models.BoardPoint,Technical.Scripts.Models.BoardPoint)
extern void BoardMatcher_TestSwipeMatch_mFCA1FF44E9BF860ABBD9388CBEC7961821350DCC (void);
// 0x0000006C System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardMatcher::CheckIfCanContinue(System.Int32[0...,0...])
extern void BoardMatcher_CheckIfCanContinue_m1ACF818F58B8155B4EC5469399D563E0FFE85A8A (void);
// 0x0000006D System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardMatcher::MatchSingleTile(System.Int32[0...,0...],Technical.Scripts.Models.BoardPoint,Technical.Scripts.Models.BoardPoint)
extern void BoardMatcher_MatchSingleTile_m887672AA832A1C07BA9CD69BA596A34C34C21D5C (void);
// 0x0000006E System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardMatcher::CheckRow(System.Int32[0...,0...],System.Int32)
extern void BoardMatcher_CheckRow_m06E9F2435C0961A3FF0496D6136117AA6F6A5B1E (void);
// 0x0000006F System.Collections.Generic.IEnumerable`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardMatcher::CheckColumn(System.Int32[0...,0...],System.Int32)
extern void BoardMatcher_CheckColumn_mFB3015E5AE81C4D84F103FC8C43754CD45568030 (void);
// 0x00000070 System.Boolean Technical.Scripts.Board.BoardMatcher::SeedsMatch(System.Int32,System.Int32)
extern void BoardMatcher_SeedsMatch_mD1357D5B78759D907F3A73666723A338D73D3B20 (void);
// 0x00000071 System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardMatcher::CheckBomb(System.Int32[0...,0...],System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint>)
extern void BoardMatcher_CheckBomb_mB4180665CEEC31FB0650728AA0008E7F5A7D9264 (void);
// 0x00000072 System.Void Technical.Scripts.Board.BoardMatcher::SwapSeeds(System.Int32[0...,0...],Technical.Scripts.Models.BoardPoint,Technical.Scripts.Models.BoardPoint)
extern void BoardMatcher_SwapSeeds_m6EE3390BB9EB5E520A392B0C50C78CEE8FD85CA9 (void);
// 0x00000073 System.Collections.Generic.List`1<Technical.Scripts.Models.BoardPoint> Technical.Scripts.Board.BoardRefiller::GetNextSeedForPosition(System.Int32[0...,0...],Technical.Scripts.Models.BoardPoint,System.Int32[],System.Single,System.Single)
extern void BoardRefiller_GetNextSeedForPosition_m17A00F17351F14C423F7B6E01BC624C0626A0A42 (void);
// 0x00000074 System.Int32 Technical.Scripts.Board.BoardRefiller::GetRandomSeed(System.Collections.Generic.IReadOnlyList`1<System.Int32>,System.Int32)
extern void BoardRefiller_GetRandomSeed_m745CCC10A58ABC78153863AA8CA111A687A388D3 (void);
// 0x00000075 System.Int32 Technical.Scripts.Board.TileController::get_Row()
extern void TileController_get_Row_mB6CD7F3484BE05A2AB6A60DEE117A8FE9F89AF32 (void);
// 0x00000076 System.Int32 Technical.Scripts.Board.TileController::get_Column()
extern void TileController_get_Column_m76BF8D7112180A708CE51A4FADCA9D421BD0F643 (void);
// 0x00000077 System.Void Technical.Scripts.Board.TileController::Awake()
extern void TileController_Awake_m06205241CC8A28C0BDAE22FED8ED8C6E5F73944C (void);
// 0x00000078 System.Void Technical.Scripts.Board.TileController::OnMouseDown()
extern void TileController_OnMouseDown_mB66A8A63FC1992EB9FE54CBD97069F3D6E6DAB18 (void);
// 0x00000079 System.String Technical.Scripts.Board.TileController::ToString()
extern void TileController_ToString_m3800945DC327D6EA5FC9C34D7BD3BA8167C9A99D (void);
// 0x0000007A System.Boolean Technical.Scripts.Board.TileController::Equals(Technical.Scripts.Board.TileController)
extern void TileController_Equals_mDEF35D6442DC0EEA07B31422EF2404D7C49CB678 (void);
// 0x0000007B System.Void Technical.Scripts.Board.TileController::Select()
extern void TileController_Select_m087F5D06C37ADD17565A53E6536A202270598F7F (void);
// 0x0000007C System.Void Technical.Scripts.Board.TileController::Deselect()
extern void TileController_Deselect_m1C52DF6AC4D498D9FC46C83F699D72BF1F918EC6 (void);
// 0x0000007D System.Void Technical.Scripts.Board.TileController::Init(System.Int32,System.Int32,Technical.Scripts.ScriptableObjects.BoardTileData,System.Single)
extern void TileController_Init_m4A5B4FE63A3736BCCCE04DB5A8C5EDF9DE1B10E2 (void);
// 0x0000007E System.Void Technical.Scripts.Board.TileController::MoveTo(UnityEngine.Vector3,System.Int32,System.Int32)
extern void TileController_MoveTo_m912663FFD0F967FEA974B30D2E09F2786A2F92D4 (void);
// 0x0000007F System.Void Technical.Scripts.Board.TileController::Shake(System.Boolean)
extern void TileController_Shake_m8A8925269F0C72A4B97041DD47F327EA041A10DA (void);
// 0x00000080 System.Collections.IEnumerator Technical.Scripts.Board.TileController::SetData(Technical.Scripts.ScriptableObjects.BoardTileData,System.Single)
extern void TileController_SetData_m411282DCDB275D09DE6B1F4E0D916EE19BD66811 (void);
// 0x00000081 System.Collections.IEnumerator Technical.Scripts.Board.TileController::MoveToPosition()
extern void TileController_MoveToPosition_m71C7D81302A073208AB364387ED99F07D49D0B94 (void);
// 0x00000082 System.Void Technical.Scripts.Board.TileController::Highlight()
extern void TileController_Highlight_m03F995EFBACFB908FCAE573836A44ABF0944DC4E (void);
// 0x00000083 System.Void Technical.Scripts.Board.TileController::RemoveHighlight()
extern void TileController_RemoveHighlight_m7D41060C872DE2F6D0432C27F70A79E99A19BE66 (void);
// 0x00000084 System.Void Technical.Scripts.Board.TileController::Matched(System.Single)
extern void TileController_Matched_m27D285739248A9C79B2391805ADE7E90D83C6E80 (void);
// 0x00000085 System.Collections.IEnumerator Technical.Scripts.Board.TileController::ExplodeAnimation(System.Single)
extern void TileController_ExplodeAnimation_m5294B489526E94C0339D761B9220F1DA299F09BF (void);
// 0x00000086 System.Void Technical.Scripts.Board.TileController::SetNewData(Technical.Scripts.ScriptableObjects.BoardTileData,System.Single)
extern void TileController_SetNewData_m65D5BADD254A0778496F7C4E461EC7DEF2EA055E (void);
// 0x00000087 System.Collections.IEnumerator Technical.Scripts.Board.TileController::ReloadTile(Technical.Scripts.ScriptableObjects.BoardTileData,System.Single)
extern void TileController_ReloadTile_m1EC08BF0D6ED7F81B08E4B14A87B4B5553949CC7 (void);
// 0x00000088 System.Void Technical.Scripts.Board.TileController::UpdateData(Technical.Scripts.ScriptableObjects.BoardTileData)
extern void TileController_UpdateData_m8EDEB8D5EDA2BF3EB75F24C00E1285341F3EA296 (void);
// 0x00000089 System.Void Technical.Scripts.Board.TileController::PlayClip(UnityEngine.AudioClip)
extern void TileController_PlayClip_mE5A75CDC18FC74B6288CACCF75F920A3155FC074 (void);
// 0x0000008A System.Void Technical.Scripts.Board.TileController::.ctor()
extern void TileController__ctor_m1BADB09E98DDCE262C7802B2FD70549018FB3F7F (void);
// 0x0000008B System.Void Technical.Scripts.Board.TileController::.cctor()
extern void TileController__cctor_mDFEE628CF437B86A320F3C0C8F85AA2C41FF44C5 (void);
// 0x0000008C Technical.Scripts.Models.PlayerData Technical.Scripts.API.PlayerDataAPI::LoadData(System.Boolean)
extern void PlayerDataAPI_LoadData_mF320AEDD4A34873555FDF67496729CC94EC3943C (void);
// 0x0000008D System.Boolean Technical.Scripts.API.PlayerDataAPI::SaveData(Technical.Scripts.Models.PlayerData,System.Boolean)
extern void PlayerDataAPI_SaveData_m32990E39C3ACA9EE3C665187BF804B5B46FDD566 (void);
// 0x0000008E System.Void Technical.Scripts.API.PlayerDataAPI::.cctor()
extern void PlayerDataAPI__cctor_m611ECC457E8CFF58D186E483A371047CC0AE059B (void);
// 0x0000008F System.Void Technical.Scripts.BoardUtils_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mD1746EA80B458FDBC171D652D3CFEC4523BF6969 (void);
// 0x00000090 System.Boolean Technical.Scripts.BoardUtils_<>c__DisplayClass0_0::<Distinct>b__0(Technical.Scripts.Models.BoardPoint)
extern void U3CU3Ec__DisplayClass0_0_U3CDistinctU3Eb__0_mECABBB528DAD5AF308F9670060C2250DB3EC8A85 (void);
// 0x00000091 System.Void Technical.Scripts.UI.LoadingScreenController_<LoadSceneAsync>d__8::.ctor(System.Int32)
extern void U3CLoadSceneAsyncU3Ed__8__ctor_m46749798724F2C33FE96CC671860CA24C9FE47CB (void);
// 0x00000092 System.Void Technical.Scripts.UI.LoadingScreenController_<LoadSceneAsync>d__8::System.IDisposable.Dispose()
extern void U3CLoadSceneAsyncU3Ed__8_System_IDisposable_Dispose_m6FAE7D473EFBA6A785FF5E6FE6ABE4AF0674A8C0 (void);
// 0x00000093 System.Boolean Technical.Scripts.UI.LoadingScreenController_<LoadSceneAsync>d__8::MoveNext()
extern void U3CLoadSceneAsyncU3Ed__8_MoveNext_m212AF6556CE07560723F11BF915BC6ADBEBD8C3A (void);
// 0x00000094 System.Object Technical.Scripts.UI.LoadingScreenController_<LoadSceneAsync>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneAsyncU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AF482089E95DCEB8DC1432980DD59FE2F7284D8 (void);
// 0x00000095 System.Void Technical.Scripts.UI.LoadingScreenController_<LoadSceneAsync>d__8::System.Collections.IEnumerator.Reset()
extern void U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_Reset_m57B5BE8B52D65C278E6286BA43E9D46A34535EC3 (void);
// 0x00000096 System.Object Technical.Scripts.UI.LoadingScreenController_<LoadSceneAsync>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_get_Current_mA132F333B6856C61F6346B98A2D297547712E1C6 (void);
// 0x00000097 System.Void Technical.Scripts.Singletons.ScoreController_<UpdateCurrentScore>d__16::.ctor(System.Int32)
extern void U3CUpdateCurrentScoreU3Ed__16__ctor_m39475BD3F8CF7098748B3F34C68CD0B5814C4116 (void);
// 0x00000098 System.Void Technical.Scripts.Singletons.ScoreController_<UpdateCurrentScore>d__16::System.IDisposable.Dispose()
extern void U3CUpdateCurrentScoreU3Ed__16_System_IDisposable_Dispose_m0AB471E48A05C73798449A425893C1192A3247EA (void);
// 0x00000099 System.Boolean Technical.Scripts.Singletons.ScoreController_<UpdateCurrentScore>d__16::MoveNext()
extern void U3CUpdateCurrentScoreU3Ed__16_MoveNext_mFB585978126E8203F371B4CDD278530B6DD64BC2 (void);
// 0x0000009A System.Object Technical.Scripts.Singletons.ScoreController_<UpdateCurrentScore>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateCurrentScoreU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6835571FCE4EC6DFE79F98C6C3ADE350913457FE (void);
// 0x0000009B System.Void Technical.Scripts.Singletons.ScoreController_<UpdateCurrentScore>d__16::System.Collections.IEnumerator.Reset()
extern void U3CUpdateCurrentScoreU3Ed__16_System_Collections_IEnumerator_Reset_mD15147FBAEB77CBAAC0E366F2EAE4D552663B080 (void);
// 0x0000009C System.Object Technical.Scripts.Singletons.ScoreController_<UpdateCurrentScore>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateCurrentScoreU3Ed__16_System_Collections_IEnumerator_get_Current_mBD9FE06F9D53F3EB4269F15711072302CB2C5A77 (void);
// 0x0000009D System.Void Technical.Scripts.Singletons.ScoreController_<UpdateComboAmount>d__18::.ctor(System.Int32)
extern void U3CUpdateComboAmountU3Ed__18__ctor_mC1DC08B4F141F61F290AE981EC0F6C90716EF9F3 (void);
// 0x0000009E System.Void Technical.Scripts.Singletons.ScoreController_<UpdateComboAmount>d__18::System.IDisposable.Dispose()
extern void U3CUpdateComboAmountU3Ed__18_System_IDisposable_Dispose_m5DD3504EC9E4301F0963A086424BE6FA0BB5C186 (void);
// 0x0000009F System.Boolean Technical.Scripts.Singletons.ScoreController_<UpdateComboAmount>d__18::MoveNext()
extern void U3CUpdateComboAmountU3Ed__18_MoveNext_m16D167F383277EABFA8F8E252CB98305A73E5C85 (void);
// 0x000000A0 System.Object Technical.Scripts.Singletons.ScoreController_<UpdateComboAmount>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateComboAmountU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE70120AD5F7566CBC553CFF0B85E528AC88317B (void);
// 0x000000A1 System.Void Technical.Scripts.Singletons.ScoreController_<UpdateComboAmount>d__18::System.Collections.IEnumerator.Reset()
extern void U3CUpdateComboAmountU3Ed__18_System_Collections_IEnumerator_Reset_mEE5CA2D61FB216CA116566E1DC66D6523491379F (void);
// 0x000000A2 System.Object Technical.Scripts.Singletons.ScoreController_<UpdateComboAmount>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateComboAmountU3Ed__18_System_Collections_IEnumerator_get_Current_m13E8B48AE908A207B1314282770CB98F397E8812 (void);
// 0x000000A3 System.Void Technical.Scripts.Singletons.SoundController_<FadeAudio>d__8::.ctor(System.Int32)
extern void U3CFadeAudioU3Ed__8__ctor_m6304997BB2B7B048E0C567702A81F9889760A60A (void);
// 0x000000A4 System.Void Technical.Scripts.Singletons.SoundController_<FadeAudio>d__8::System.IDisposable.Dispose()
extern void U3CFadeAudioU3Ed__8_System_IDisposable_Dispose_m259097C41FB928D9349958F7BC78A2299457B3BB (void);
// 0x000000A5 System.Boolean Technical.Scripts.Singletons.SoundController_<FadeAudio>d__8::MoveNext()
extern void U3CFadeAudioU3Ed__8_MoveNext_m46C30906B463D275D841F25D42B29C325DB95556 (void);
// 0x000000A6 System.Object Technical.Scripts.Singletons.SoundController_<FadeAudio>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeAudioU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7676E3E5E751120F7AC30665197B18A0655778FF (void);
// 0x000000A7 System.Void Technical.Scripts.Singletons.SoundController_<FadeAudio>d__8::System.Collections.IEnumerator.Reset()
extern void U3CFadeAudioU3Ed__8_System_Collections_IEnumerator_Reset_mF9C2B86E283948FEF6173973F94A704A94B19C7B (void);
// 0x000000A8 System.Object Technical.Scripts.Singletons.SoundController_<FadeAudio>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CFadeAudioU3Ed__8_System_Collections_IEnumerator_get_Current_m11D47A13B58AF95EC8E74019A8AB972FA943A2D3 (void);
// 0x000000A9 System.Void Technical.Scripts.Board.BoardMatcher_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mC2343FDDFB4A4D484064949490A63DD1ED4D8F6B (void);
// 0x000000AA System.Boolean Technical.Scripts.Board.BoardMatcher_<>c__DisplayClass7_0::<CheckBomb>b__0(Technical.Scripts.Models.BoardPoint)
extern void U3CU3Ec__DisplayClass7_0_U3CCheckBombU3Eb__0_m13BFDB9DCB4819DDB3F4936A0392C573DB2AE8B4 (void);
// 0x000000AB System.Void Technical.Scripts.Board.TileController_<SetData>d__29::.ctor(System.Int32)
extern void U3CSetDataU3Ed__29__ctor_m468CBED0A0A66A633827928AAB9E5A55535D84FE (void);
// 0x000000AC System.Void Technical.Scripts.Board.TileController_<SetData>d__29::System.IDisposable.Dispose()
extern void U3CSetDataU3Ed__29_System_IDisposable_Dispose_m2BEEB3B32CCAD0012E961E4CE9146A09261194CD (void);
// 0x000000AD System.Boolean Technical.Scripts.Board.TileController_<SetData>d__29::MoveNext()
extern void U3CSetDataU3Ed__29_MoveNext_m29957D71D7B366728E96A796D3FA250CEFF635EE (void);
// 0x000000AE System.Object Technical.Scripts.Board.TileController_<SetData>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetDataU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C284FA1C38716E6DC420AC13B100F964C775476 (void);
// 0x000000AF System.Void Technical.Scripts.Board.TileController_<SetData>d__29::System.Collections.IEnumerator.Reset()
extern void U3CSetDataU3Ed__29_System_Collections_IEnumerator_Reset_m61DD2386CB6CB26ED1309C8C69050D3302B8D42F (void);
// 0x000000B0 System.Object Technical.Scripts.Board.TileController_<SetData>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CSetDataU3Ed__29_System_Collections_IEnumerator_get_Current_mAB6F7A6AFDC4FE709855011849C67948B504CFEE (void);
// 0x000000B1 System.Void Technical.Scripts.Board.TileController_<MoveToPosition>d__30::.ctor(System.Int32)
extern void U3CMoveToPositionU3Ed__30__ctor_mD9BEDF6229B08FAB79E1A58035168605F93AD133 (void);
// 0x000000B2 System.Void Technical.Scripts.Board.TileController_<MoveToPosition>d__30::System.IDisposable.Dispose()
extern void U3CMoveToPositionU3Ed__30_System_IDisposable_Dispose_m082323E926968B2E62F9D9320E5AAE008F22947E (void);
// 0x000000B3 System.Boolean Technical.Scripts.Board.TileController_<MoveToPosition>d__30::MoveNext()
extern void U3CMoveToPositionU3Ed__30_MoveNext_mE068839FE60524255A366E5041208ED6B1B34758 (void);
// 0x000000B4 System.Object Technical.Scripts.Board.TileController_<MoveToPosition>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveToPositionU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B44B6222CB68EF7CB64EA36767ABA0CA565AD80 (void);
// 0x000000B5 System.Void Technical.Scripts.Board.TileController_<MoveToPosition>d__30::System.Collections.IEnumerator.Reset()
extern void U3CMoveToPositionU3Ed__30_System_Collections_IEnumerator_Reset_mBD0AE318351656FE2C6B62FC958B176408E42862 (void);
// 0x000000B6 System.Object Technical.Scripts.Board.TileController_<MoveToPosition>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CMoveToPositionU3Ed__30_System_Collections_IEnumerator_get_Current_m15726E18AE848C79A0CE6A7A0C64DA32910D08FA (void);
// 0x000000B7 System.Void Technical.Scripts.Board.TileController_<ExplodeAnimation>d__34::.ctor(System.Int32)
extern void U3CExplodeAnimationU3Ed__34__ctor_m88EB0FF6AB6DAC1D29B2857AB2594D0BE6CA9C59 (void);
// 0x000000B8 System.Void Technical.Scripts.Board.TileController_<ExplodeAnimation>d__34::System.IDisposable.Dispose()
extern void U3CExplodeAnimationU3Ed__34_System_IDisposable_Dispose_m139E56A0E2E0222A134B0778324F024C7366A94E (void);
// 0x000000B9 System.Boolean Technical.Scripts.Board.TileController_<ExplodeAnimation>d__34::MoveNext()
extern void U3CExplodeAnimationU3Ed__34_MoveNext_m36E8D3C7370979A9A76354DAB238250A0A2EC87E (void);
// 0x000000BA System.Object Technical.Scripts.Board.TileController_<ExplodeAnimation>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeAnimationU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE58241B6CA7B482489DDDD97B397BD9154A9A48 (void);
// 0x000000BB System.Void Technical.Scripts.Board.TileController_<ExplodeAnimation>d__34::System.Collections.IEnumerator.Reset()
extern void U3CExplodeAnimationU3Ed__34_System_Collections_IEnumerator_Reset_m33D1DEAA52BA16F68FE54A7A8E61F2799F22CCDB (void);
// 0x000000BC System.Object Technical.Scripts.Board.TileController_<ExplodeAnimation>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeAnimationU3Ed__34_System_Collections_IEnumerator_get_Current_m72453571FFD8B58062A12BD791B06CC09742805A (void);
// 0x000000BD System.Void Technical.Scripts.Board.TileController_<ReloadTile>d__36::.ctor(System.Int32)
extern void U3CReloadTileU3Ed__36__ctor_mE24B3C9687567302F4C051D548C820F9F9A6A3D0 (void);
// 0x000000BE System.Void Technical.Scripts.Board.TileController_<ReloadTile>d__36::System.IDisposable.Dispose()
extern void U3CReloadTileU3Ed__36_System_IDisposable_Dispose_m4CFF7A0CB589A7C0753850ACB18E2E84B52025FB (void);
// 0x000000BF System.Boolean Technical.Scripts.Board.TileController_<ReloadTile>d__36::MoveNext()
extern void U3CReloadTileU3Ed__36_MoveNext_m74BD2AD9C05FF193FB5772AD1D7F7B0CD2F27804 (void);
// 0x000000C0 System.Object Technical.Scripts.Board.TileController_<ReloadTile>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadTileU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1D66EBD72C69002F1EF6F47E87AC9EDE08CAC67 (void);
// 0x000000C1 System.Void Technical.Scripts.Board.TileController_<ReloadTile>d__36::System.Collections.IEnumerator.Reset()
extern void U3CReloadTileU3Ed__36_System_Collections_IEnumerator_Reset_mC8458827A096E6B65E403A5D2BD0E09EAE142E96 (void);
// 0x000000C2 System.Object Technical.Scripts.Board.TileController_<ReloadTile>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CReloadTileU3Ed__36_System_Collections_IEnumerator_get_Current_mBA071B59BC2A6E067EF7B3EB68025E550C415EF5 (void);
static Il2CppMethodPointer s_methodPointers[194] = 
{
	ScoreCounterController_Awake_m3B17D22850C963EF93E0D3CCD4B7347DA645E385,
	ScoreCounterController_UpdateText_mB49C90C8445F77A3ECDBE49931F48F15E8A07480,
	ScoreCounterController__ctor_mC0994BD213E8F80B38C72F611B9129E55336B006,
	ScoreCounterController__cctor_mB9AF772DC0B169BC8299C3B0AAF35B53FDA9775A,
	NULL,
	ArrayExtensions__cctor_m390EC716F5FC7245214A35EF87591D9F6488C78E,
	BoardUtils_Distinct_m86CA8B299979691D5ECCCFE46E3139CCDA606F72,
	CameraResolutionHandler_Awake_mE76810C130CC2ACCF53F3C779C27E52DB2BAD7D3,
	CameraResolutionHandler_OnPreCull_m1F34C250A47384D63A32F3214CA9421D02F7F2BA,
	CameraResolutionHandler_Start_m5CBE0D7630C01C73CA742BA6F890656C755FFE64,
	CameraResolutionHandler_RescaleCamera_m27551C9A4CDF7809C6298775A5BB0A52DF02DD59,
	CameraResolutionHandler__ctor_m21F20ACB7622085517FED78D6435DEC851152437,
	ComboCounterController_Awake_m282B243EA414E6D833CCB72C8F6DE04F63866C02,
	ComboCounterController_UpdateCombo_m6C9C0D15802D462F8AB45510DB2CB03757E77D81,
	ComboCounterController_SetEmphasisText_mCFBAEB1D4BFFC8B3B65AECB41162FC446CBE569F,
	ComboCounterController__ctor_m678226EF12D899572DA001AEF0B76C2709192BF5,
	ComboCounterController__cctor_mE816BF2158BB920F00CEB41BA8BD7E2E8B87FD28,
	GameUIController_Start_m4AD9B043028771D06F0829618B94FF957033C17A,
	GameUIController_OnTimerFreeze_m28C516D130823C61FBE2C1F74AEFA23BC63EC3FF,
	GameUIController_OnDestroy_m76DB9CE39C901F8E06E660C2447E39553D819E5F,
	GameUIController_TogglePauseMenu_m45D56E80C69A62DA86616D9640C17C49BC08B0ED,
	GameUIController_UpdateTimerText_m134EBBC46FC2AAF12939D14DA94839A231381028,
	GameUIController_ShowEndGameUI_m9B0128D10E4E6BA1702BBA60D288F8FAEDD7A26B,
	GameUIController_BackToHomePage_m49EF78AA1EF29F306EA054689EA42F00ED42F20F,
	GameUIController_RestartGame_m42D7F371181FE0DB4F8DF0856EE150AD32562CEB,
	GameUIController_UpdateScore_m4ED5CDB777F0BFDA52B0825F55CDBA357AB47CF9,
	GameUIController_UpdateCombo_mC48E0E9D9C5DD891D738D5E64DA3A55A5F9F2539,
	GameUIController__ctor_m210C91EA10F399BB108C74E92A2349DE0DE7CCB5,
	InteractableButtonController_Awake_mA02B7468A7E16ADD8F5760985BB01E65BF2CCAD1,
	InteractableButtonController_OnPointerDown_m56ADAB22EE21D5454F938FAA9A223B51ED2CE61A,
	InteractableButtonController_OnPointerUp_m8C93F6CD461C18440F0D0407DFA6FE650CB78A1A,
	InteractableButtonController_OnPointerEnter_m5BBEBF8EFB4B43E48E77688500C6B675EA51847C,
	InteractableButtonController_OnPointerExit_m6BF56BC26672242B477992958A8F6BC64D11C81F,
	InteractableButtonController_OnPointerClick_m209AEB211650A64C624B303623162DF220D4CE10,
	InteractableButtonController__ctor_mCCC06E85F217757E3E17255683093E7E3C2072B0,
	LoadingScreenController_Awake_m5E3E7937AA30A0F28DB59A66F3AAA540C47395BE,
	LoadingScreenController_Show_m7F322D9C01CBF5AE84A60DE62B0AE20A6B2578AE,
	LoadingScreenController_Hide_m394D863FFBDE079BA649920421DA2A710AA7CCF4,
	LoadingScreenController_LoadSceneAsync_m0D1A4B3C8755505B22D5268F5D95D79497AEEECF,
	LoadingScreenController__ctor_mF4D53622A4345FAED66C3BA97E24A41E56EAB407,
	LoadingScreenController__cctor_m595EF24449F9922460763A06C4C0D21097D21506,
	MainUIController_Awake_m716197C8BA8F58BEB95A7BE85FC0BA99D864ADF9,
	MainUIController_Start_mDA7E37D8CBE3F01B259A6A2357F55EB9FFA17E85,
	MainUIController_SelectGameMode_mE0E537BF8D138A377303715E2CEC10925B34BA5D,
	MainUIController_PlayNewGame_m898A4EEB07E33D0F869323FDA9727AFA40AF5585,
	MainUIController__ctor_m71E0B2A5167F8FD6D2BB6FAA9247D785D1534E50,
	MainUIController__cctor_m93855953C314C8C7563F1E55836510C910DABE07,
	TimerEvent__ctor_m68CC9A30E7499EFCC4E492F9C22651D8A6CBDF79,
	TimerController_Start_m02335138F6469881B1ED42BBE76738E652AE1E2C,
	TimerController_Update_m8787EF5EB60AA8C0A81BE1B8AAC82871FE611757,
	TimerController_Freeze_m5B5FF2F202D719E05C20E65987AA89D8778D01A5,
	TimerController__ctor_m0FBD89E8529AA430F332A84E70941177BE53A8ED,
	GameController_get_timer_m89A2ED7BB0CFA358EA132989FD916C2631DCAD32,
	GameController_Awake_mE3FA01A55D6E645F524D18C9CF140CA44AF805B5,
	GameController_Start_mECC7E53B35E7AEF76CFFDD084DF567DCCF9D44A8,
	GameController_StartGame_mBE99B45447C9C25F3F08ED301775AB3A7D839E8B,
	GameController_EndGame_mFB8ACA7262127E44A0BFDD65A1819A94FA790399,
	GameController_AfterSceneLoaded_m75F4FD03A3295D5B1D5F95C2D6644640C1FD878C,
	GameController_BackToMainScene_m4829558F5A3EB76D078DA5672E9483E3E8B85B43,
	GameController_ReloadGameScene_m0D97887140E059EEBAA0BA6259E25EDAB2D561B0,
	GameController_TogglePause_mB470177B9E046E514437F51666C2139119BBE460,
	GameController_FreezeTime_m2F35B57C99D146CB199255CFC788F1AB32D56372,
	GameController_PlayMusicByScene_m4A8B1E07365FE960FAF499CC714C0D66934ADB79,
	GameController__ctor_mE04C9DC0F13365C53B13332D0847E8655C4C53E6,
	GameController__cctor_mD80FDAAD0770B4735E6B688A5470C6027E223E36,
	ScoreController_get_score_mB3F8668DAB993EEC28D26AEAC267238FF7B69BDA,
	ScoreController_Awake_m6E14058A26132A5D9B3A7C64733624DDEC120A33,
	ScoreController_Start_mC7FCAB4251B3671E2DF56456862EA48145463FCF,
	ScoreController_OnDestroy_m033A4C3B11382DEE724B60E7241F346603FF09DC,
	ScoreController_ComboMatched_m31E9F24C201D4C66AB8557003B134110E0624464,
	ScoreController_UpdateCurrentScore_m140BA6C8BB7CDF26281435EAD2E6C78A937ADA85,
	ScoreController_ResetCombo_mE1CCA142E954B02A6A5A6DAFCB2F3EB2068D4985,
	ScoreController_UpdateComboAmount_mCAD7A17AED5EEFD63E2F386F6C30BCB5B237AF6C,
	ScoreController__ctor_m2A9D1CB9D9D26076F614794377B245C0AFE09312,
	SoundController_Awake_m91F5439268082D6DD97777E1248AA65192349654,
	SoundController_PlayMusicByScene_mACA9E8E80C1990EB3B94235DEE2DC29E2232E8BB,
	SoundController_PlayMainMusic_mEBFD0D918A78011C379039835AFE412C28D14EC5,
	SoundController_PlayInGameMusic_m75E34C3C77D7827A39DE445015DA91C3B1E24304,
	SoundController_FadeAudio_mCE7BA2DF89C56A5CCFD02A3B401A6B660CEBE669,
	SoundController__ctor_m4C13E014B9D4687951E2D29D010E630482A5556E,
	TileAnimationLock_get_Locked_m2A543847ACA1A333501A7A2A19261C2146EA08A7,
	TileAnimationLock_Awake_m3701D8E337419D2B92FB5372BA58992DC888EEDC,
	TileAnimationLock_Lock_mD47DA611EC236E75ACE2E793E80ED9029AA19617,
	TileAnimationLock_Unlock_m99CA531047BB60F6F062EEF8B288A35350111409,
	TileAnimationLock__ctor_m8F76B8DDE19781DEDB521A4BB4C99F98A6BB2678,
	BoardTileData__ctor_mE26CD074659A96D789636F0601CBC59787ACEEB5,
	BoardPoint__ctor_mB8E9490AEE1BFC9D1E90C7F57388A2993DC9B4DF,
	BoardPoint_Equals_m5907168A1A3587A55E4E3AEB019C00EFC9D296F8,
	PlayerData__ctor_mA6134AE624F63343075609F6DD2636AA72148CFC,
	BoardController_Start_m153FF4C86BC4C96435F35AC0A3C70C6751656960,
	BoardController_OnDestroy_mA0F24A072AD362EB217E3918AC0406DC106F27F2,
	BoardController_GenerateBoard_mAF6E88394B067F7A4914ADA9A9064C1C803ED669,
	BoardController_GenerateTiles_mD1221F07D6CFA52B57D89E85B3CE8ADBABAC0914,
	BoardController_TileClick_m4328556BD27A91DEA516D9F282984A1D39BB0D96,
	BoardController_CheckMatch_mF85D75AB6A325B16FE9C12B89956A9AD9329214F,
	BoardController_MatchAll_m9EF10DC76D7695C9C76361A72720AE8CEF8077F2,
	BoardController_RefillAndMatch_m541E711B66BEDE2519372BE72C43DC99A20E5C20,
	BoardController_PerformMatch_mDAFDCE8AC8B0E76C083C54F8D30445D4B20083ED,
	BoardController_PerformRefill_mC839EFC57D485EC0A3AA6C7E47E867A784EA41ED,
	BoardController_GetDataForSeed_m04F781F158E6E73863569DA7921EAB5C5B3BE50B,
	BoardController_ClearHighlightedTiles_m2B1BCD4EE0CBC332566D684958E8F2AB918B6E4E,
	BoardController_StopAll_m8957B5E80928A2F48D14DE44789350F0DCCF0C9C,
	BoardController__ctor_mF3DBF37CA12DA3DFCD0CA1E876E5D792D2104914,
	BoardGenerator_CreateMatrix_m2E94DFBCCA79D588A5FCB8B75F4E735BC63070F7,
	BoardGenerator_GetRandomSeedsByRow_mC4473F36CDA41733A69B7EB51416A4AF55EF298E,
	BoardMatcher_MatchAll_m767E976934D0C15FCE28C92DE7C9598DBD96D22F,
	BoardMatcher_TestSwipeMatch_mFCA1FF44E9BF860ABBD9388CBEC7961821350DCC,
	BoardMatcher_CheckIfCanContinue_m1ACF818F58B8155B4EC5469399D563E0FFE85A8A,
	BoardMatcher_MatchSingleTile_m887672AA832A1C07BA9CD69BA596A34C34C21D5C,
	BoardMatcher_CheckRow_m06E9F2435C0961A3FF0496D6136117AA6F6A5B1E,
	BoardMatcher_CheckColumn_mFB3015E5AE81C4D84F103FC8C43754CD45568030,
	BoardMatcher_SeedsMatch_mD1357D5B78759D907F3A73666723A338D73D3B20,
	BoardMatcher_CheckBomb_mB4180665CEEC31FB0650728AA0008E7F5A7D9264,
	BoardMatcher_SwapSeeds_m6EE3390BB9EB5E520A392B0C50C78CEE8FD85CA9,
	BoardRefiller_GetNextSeedForPosition_m17A00F17351F14C423F7B6E01BC624C0626A0A42,
	BoardRefiller_GetRandomSeed_m745CCC10A58ABC78153863AA8CA111A687A388D3,
	TileController_get_Row_mB6CD7F3484BE05A2AB6A60DEE117A8FE9F89AF32,
	TileController_get_Column_m76BF8D7112180A708CE51A4FADCA9D421BD0F643,
	TileController_Awake_m06205241CC8A28C0BDAE22FED8ED8C6E5F73944C,
	TileController_OnMouseDown_mB66A8A63FC1992EB9FE54CBD97069F3D6E6DAB18,
	TileController_ToString_m3800945DC327D6EA5FC9C34D7BD3BA8167C9A99D,
	TileController_Equals_mDEF35D6442DC0EEA07B31422EF2404D7C49CB678,
	TileController_Select_m087F5D06C37ADD17565A53E6536A202270598F7F,
	TileController_Deselect_m1C52DF6AC4D498D9FC46C83F699D72BF1F918EC6,
	TileController_Init_m4A5B4FE63A3736BCCCE04DB5A8C5EDF9DE1B10E2,
	TileController_MoveTo_m912663FFD0F967FEA974B30D2E09F2786A2F92D4,
	TileController_Shake_m8A8925269F0C72A4B97041DD47F327EA041A10DA,
	TileController_SetData_m411282DCDB275D09DE6B1F4E0D916EE19BD66811,
	TileController_MoveToPosition_m71C7D81302A073208AB364387ED99F07D49D0B94,
	TileController_Highlight_m03F995EFBACFB908FCAE573836A44ABF0944DC4E,
	TileController_RemoveHighlight_m7D41060C872DE2F6D0432C27F70A79E99A19BE66,
	TileController_Matched_m27D285739248A9C79B2391805ADE7E90D83C6E80,
	TileController_ExplodeAnimation_m5294B489526E94C0339D761B9220F1DA299F09BF,
	TileController_SetNewData_m65D5BADD254A0778496F7C4E461EC7DEF2EA055E,
	TileController_ReloadTile_m1EC08BF0D6ED7F81B08E4B14A87B4B5553949CC7,
	TileController_UpdateData_m8EDEB8D5EDA2BF3EB75F24C00E1285341F3EA296,
	TileController_PlayClip_mE5A75CDC18FC74B6288CACCF75F920A3155FC074,
	TileController__ctor_m1BADB09E98DDCE262C7802B2FD70549018FB3F7F,
	TileController__cctor_mDFEE628CF437B86A320F3C0C8F85AA2C41FF44C5,
	PlayerDataAPI_LoadData_mF320AEDD4A34873555FDF67496729CC94EC3943C,
	PlayerDataAPI_SaveData_m32990E39C3ACA9EE3C665187BF804B5B46FDD566,
	PlayerDataAPI__cctor_m611ECC457E8CFF58D186E483A371047CC0AE059B,
	U3CU3Ec__DisplayClass0_0__ctor_mD1746EA80B458FDBC171D652D3CFEC4523BF6969,
	U3CU3Ec__DisplayClass0_0_U3CDistinctU3Eb__0_mECABBB528DAD5AF308F9670060C2250DB3EC8A85,
	U3CLoadSceneAsyncU3Ed__8__ctor_m46749798724F2C33FE96CC671860CA24C9FE47CB,
	U3CLoadSceneAsyncU3Ed__8_System_IDisposable_Dispose_m6FAE7D473EFBA6A785FF5E6FE6ABE4AF0674A8C0,
	U3CLoadSceneAsyncU3Ed__8_MoveNext_m212AF6556CE07560723F11BF915BC6ADBEBD8C3A,
	U3CLoadSceneAsyncU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AF482089E95DCEB8DC1432980DD59FE2F7284D8,
	U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_Reset_m57B5BE8B52D65C278E6286BA43E9D46A34535EC3,
	U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_get_Current_mA132F333B6856C61F6346B98A2D297547712E1C6,
	U3CUpdateCurrentScoreU3Ed__16__ctor_m39475BD3F8CF7098748B3F34C68CD0B5814C4116,
	U3CUpdateCurrentScoreU3Ed__16_System_IDisposable_Dispose_m0AB471E48A05C73798449A425893C1192A3247EA,
	U3CUpdateCurrentScoreU3Ed__16_MoveNext_mFB585978126E8203F371B4CDD278530B6DD64BC2,
	U3CUpdateCurrentScoreU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6835571FCE4EC6DFE79F98C6C3ADE350913457FE,
	U3CUpdateCurrentScoreU3Ed__16_System_Collections_IEnumerator_Reset_mD15147FBAEB77CBAAC0E366F2EAE4D552663B080,
	U3CUpdateCurrentScoreU3Ed__16_System_Collections_IEnumerator_get_Current_mBD9FE06F9D53F3EB4269F15711072302CB2C5A77,
	U3CUpdateComboAmountU3Ed__18__ctor_mC1DC08B4F141F61F290AE981EC0F6C90716EF9F3,
	U3CUpdateComboAmountU3Ed__18_System_IDisposable_Dispose_m5DD3504EC9E4301F0963A086424BE6FA0BB5C186,
	U3CUpdateComboAmountU3Ed__18_MoveNext_m16D167F383277EABFA8F8E252CB98305A73E5C85,
	U3CUpdateComboAmountU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE70120AD5F7566CBC553CFF0B85E528AC88317B,
	U3CUpdateComboAmountU3Ed__18_System_Collections_IEnumerator_Reset_mEE5CA2D61FB216CA116566E1DC66D6523491379F,
	U3CUpdateComboAmountU3Ed__18_System_Collections_IEnumerator_get_Current_m13E8B48AE908A207B1314282770CB98F397E8812,
	U3CFadeAudioU3Ed__8__ctor_m6304997BB2B7B048E0C567702A81F9889760A60A,
	U3CFadeAudioU3Ed__8_System_IDisposable_Dispose_m259097C41FB928D9349958F7BC78A2299457B3BB,
	U3CFadeAudioU3Ed__8_MoveNext_m46C30906B463D275D841F25D42B29C325DB95556,
	U3CFadeAudioU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7676E3E5E751120F7AC30665197B18A0655778FF,
	U3CFadeAudioU3Ed__8_System_Collections_IEnumerator_Reset_mF9C2B86E283948FEF6173973F94A704A94B19C7B,
	U3CFadeAudioU3Ed__8_System_Collections_IEnumerator_get_Current_m11D47A13B58AF95EC8E74019A8AB972FA943A2D3,
	U3CU3Ec__DisplayClass7_0__ctor_mC2343FDDFB4A4D484064949490A63DD1ED4D8F6B,
	U3CU3Ec__DisplayClass7_0_U3CCheckBombU3Eb__0_m13BFDB9DCB4819DDB3F4936A0392C573DB2AE8B4,
	U3CSetDataU3Ed__29__ctor_m468CBED0A0A66A633827928AAB9E5A55535D84FE,
	U3CSetDataU3Ed__29_System_IDisposable_Dispose_m2BEEB3B32CCAD0012E961E4CE9146A09261194CD,
	U3CSetDataU3Ed__29_MoveNext_m29957D71D7B366728E96A796D3FA250CEFF635EE,
	U3CSetDataU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C284FA1C38716E6DC420AC13B100F964C775476,
	U3CSetDataU3Ed__29_System_Collections_IEnumerator_Reset_m61DD2386CB6CB26ED1309C8C69050D3302B8D42F,
	U3CSetDataU3Ed__29_System_Collections_IEnumerator_get_Current_mAB6F7A6AFDC4FE709855011849C67948B504CFEE,
	U3CMoveToPositionU3Ed__30__ctor_mD9BEDF6229B08FAB79E1A58035168605F93AD133,
	U3CMoveToPositionU3Ed__30_System_IDisposable_Dispose_m082323E926968B2E62F9D9320E5AAE008F22947E,
	U3CMoveToPositionU3Ed__30_MoveNext_mE068839FE60524255A366E5041208ED6B1B34758,
	U3CMoveToPositionU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B44B6222CB68EF7CB64EA36767ABA0CA565AD80,
	U3CMoveToPositionU3Ed__30_System_Collections_IEnumerator_Reset_mBD0AE318351656FE2C6B62FC958B176408E42862,
	U3CMoveToPositionU3Ed__30_System_Collections_IEnumerator_get_Current_m15726E18AE848C79A0CE6A7A0C64DA32910D08FA,
	U3CExplodeAnimationU3Ed__34__ctor_m88EB0FF6AB6DAC1D29B2857AB2594D0BE6CA9C59,
	U3CExplodeAnimationU3Ed__34_System_IDisposable_Dispose_m139E56A0E2E0222A134B0778324F024C7366A94E,
	U3CExplodeAnimationU3Ed__34_MoveNext_m36E8D3C7370979A9A76354DAB238250A0A2EC87E,
	U3CExplodeAnimationU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE58241B6CA7B482489DDDD97B397BD9154A9A48,
	U3CExplodeAnimationU3Ed__34_System_Collections_IEnumerator_Reset_m33D1DEAA52BA16F68FE54A7A8E61F2799F22CCDB,
	U3CExplodeAnimationU3Ed__34_System_Collections_IEnumerator_get_Current_m72453571FFD8B58062A12BD791B06CC09742805A,
	U3CReloadTileU3Ed__36__ctor_mE24B3C9687567302F4C051D548C820F9F9A6A3D0,
	U3CReloadTileU3Ed__36_System_IDisposable_Dispose_m4CFF7A0CB589A7C0753850ACB18E2E84B52025FB,
	U3CReloadTileU3Ed__36_MoveNext_m74BD2AD9C05FF193FB5772AD1D7F7B0CD2F27804,
	U3CReloadTileU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1D66EBD72C69002F1EF6F47E87AC9EDE08CAC67,
	U3CReloadTileU3Ed__36_System_Collections_IEnumerator_Reset_mC8458827A096E6B65E403A5D2BD0E09EAE142E96,
	U3CReloadTileU3Ed__36_System_Collections_IEnumerator_get_Current_mBA071B59BC2A6E067EF7B3EB68025E550C415EF5,
};
static const int32_t s_InvokerIndices[194] = 
{
	23,
	26,
	23,
	3,
	-1,
	3,
	0,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	3,
	23,
	275,
	23,
	23,
	275,
	31,
	23,
	23,
	32,
	32,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	121,
	3,
	34,
	23,
	3,
	23,
	23,
	31,
	23,
	23,
	3,
	23,
	23,
	23,
	275,
	23,
	4,
	23,
	23,
	767,
	767,
	3,
	3,
	3,
	767,
	1161,
	23,
	23,
	3,
	1035,
	23,
	23,
	23,
	121,
	14,
	23,
	14,
	23,
	23,
	3,
	3,
	767,
	1432,
	23,
	49,
	23,
	3,
	3,
	23,
	23,
	157,
	9,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	26,
	23,
	34,
	23,
	23,
	23,
	1657,
	1658,
	502,
	2,
	0,
	2,
	152,
	152,
	53,
	1,
	144,
	1659,
	171,
	10,
	10,
	23,
	23,
	14,
	9,
	23,
	23,
	1660,
	1661,
	31,
	1482,
	14,
	23,
	23,
	275,
	1470,
	833,
	1482,
	26,
	26,
	23,
	3,
	733,
	555,
	3,
	23,
	9,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	9,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_GameAssemblyCodeGenModule;
const Il2CppCodeGenModule g_GameAssemblyCodeGenModule = 
{
	"GameAssembly.dll",
	194,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
